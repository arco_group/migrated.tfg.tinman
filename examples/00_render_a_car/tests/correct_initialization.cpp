// -*- coding:utf-8; tab-width:4; mode:cpp -*-
#include <bandit/bandit.h>

#include <fstream>
#include <string>
#include <iostream>

#include "game.h"


using namespace bandit;

go_bandit([]() {


    describe("a game", []() {
        std::ifstream game_log;
        std::vector<std::string> messages = {"Ogre::Root initialized", "Resources loaded",
                                             "SceneManager initialized",
                                             "Ogre::Camera initialized",
                                             "Ogre::Light initialized", "Car added",
                                             "Car graphic body initialized",
                                             "Car physic body initialized", "Car initialized",
                                             "RenderLoop started", "RenderLoop ended"};

        before_each([&]() {
            Log log;
            Game::shared game;

            game->new_session();
            game->session_->add(std::make_shared<Car>());
            game->session_->realize();

            int seconds = 1;
            game->loop(seconds);

            log.flush();
            game_log.open("config/game.log");
          });

        it("should log information",[&]() {
            std::string message;
            for (int i = 0; std::getline(game_log, message); ++i) {
              Assert::That(message, Equals(messages[i]));
            }

            game_log.close();
          });
      });

  });

int
main(int argc, char *argv[]) {
  return bandit::run(argc, argv);
}
