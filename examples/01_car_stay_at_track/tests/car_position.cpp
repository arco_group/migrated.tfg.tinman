// -*- coding:utf-8; tab-width:4; mode:cpp -*-
#include <bandit/bandit.h>

#include "tinman.h"

using namespace bandit;

go_bandit([] (){
    describe("a car", []() {
        Log log;
        Game game;

        before_each([&]() {
            game.new_session();

            CarController::shared bot = std::make_shared<DummyController>();
            game.session_->add(std::make_shared<Car>(bot));

            Race::shared race = std::make_shared<Race>();
            race->add(std::make_shared<Track>());
            game.session_->add(race);

            game.session_->realize();

            int frames = 60;
            game.loop(frames);
            log.flush();
          });

        it("always stay inside the track", [&]() {
            Assert::That(game.session_->car_->get_position().y, Is().Not().LessThan(0));

            game.session_->shutdown();

          });
      });
  });

int
main(int argc, char *argv[]) {
  return bandit::run(argc, argv);
}
