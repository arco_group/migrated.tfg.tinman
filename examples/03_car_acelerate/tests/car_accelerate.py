#!/usr/bin/python
# -*- coding: utf-8; mode: python -*-
import unittest
import time
import os
import sys

from commodity.os_ import SubProcess
from commodity.testing import assert_that, wait_that
from commodity.net import localhost, listen_port, Host

import Ice
Ice.loadSlice('../../src/network/tinman.ice -I/usr/share/Ice-3.5.1/slice')
import Tinman

def truncate(number, decimals):
    result = number * decimals
    result = int(number)
    return result / decimals

class RemoteGame(object):
    def __init__(self):
        broker = Ice.initialize()
        proxy = broker.stringToProxy("iPlayer0: tcp -h 127.0.0.1 -p 10000")
        self.Icar = Tinman.InstrumentatedCarPrx.checkedCast(proxy)


class TestCarMoving(unittest.TestCase):
    def setUp(self):
        server = SubProcess("./main --players 1 --track config/circuit.data --frames 600", stdout=sys.stdout)
        server.start()
        wait_that(localhost, listen_port(10000))
        self.addCleanup(server.terminate)

    def test_1(self):
        self.game = RemoteGame()
        start = self.game.Icar.getPosition()
        print "start: ", start

        self.game.Icar._exec(Tinman.Action.UseNitro)
        time.sleep(0.1)

        finish = self.game.Icar.getPosition()
        print "finish: ", finish
        self.assertNotEqual(int(start.x), int(finish.x))
