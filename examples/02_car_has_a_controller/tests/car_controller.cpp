// -*- coding:utf-8; tab-width:4; mode:cpp -*-
#include <bandit/bandit.h>
#include "tinman.h"

using namespace bandit;

go_bandit([] () {
    describe("car controller", []() {
        Log log;
        Game game;

        before_each([&]() {
            game.new_session();



            Race::shared race = std::make_shared<Race>();
            race->add(std::make_shared<Track>());
            game.session_->add(race);
          });

        it("can be pre-programmed", [&]() {
            DummyController::Command orders;
            orders = {std::make_pair("backward", 1),
                      std::make_pair("fordward", 1.5),
                      std::make_pair("turn-right", 1.2)};

            DummyController::shared bot = std::make_shared<DummyController>();
            bot->add(orders);

            game.session_->add(std::make_shared<Car>(bot));
            game.session_->realize();

            int frames = 240;
            game.loop(frames);
            log.flush();

            Assert::That(game.session_->car_->get_position().x, Is().Not().EqualTo(0));
            Assert::That(game.session_->car_->get_position().z, Is().Not().EqualTo(0));

          });
      });
  });

int main(int argc, char *argv[]) {
  return bandit::run(argc, argv);
}
