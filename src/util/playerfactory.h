#ifndef PLAYERFACTORY_H
#define PLAYERFACTORY_H
#include "race.h"
#include "player.h"

class PlayerFactory {
 public:
  enum class Type{Human, Bot, Network, Debug};
  static Player::shared create(Type type);
};

class HumanFactory{
public:
  static Player::shared create(std::string nick, EventListener::shared input,
                               Sound::shared sound, Animation::shared animation,
			       Car::shared car);
};

class BotFactory{
public:
  static Player::shared create(std::string nick, Physics::shared physics,
			       Sound::shared sound, Animation::shared animation,
			       Race::shared race, Car::shared car);
};

class NetworkFactory {
public:
  static Player::shared create(std::string nick, Sound::shared sound,
			       Animation::shared animation, Car::shared car);
};

class InstrumentatedFactory {
public:
  static Player::shared create(std::string nick, Sound::shared sound,
			       Animation::shared animation, Car::shared car);
};
#endif
