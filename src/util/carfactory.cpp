// -*- coding: utf-8; mode: c++; tab-width: 4 -*-
#include "carfactory.h"
#include <Ice/Ice.h>

CarFactory::CarFactory() {
  instrumentated_ = false;
}

CarFactory::CarFactory(Ice::ObjectAdapterPtr adapter) {
  adapter_ = adapter;
  instrumentated_ = true;
}

CarFactory::~CarFactory() {}

Car::shared
CarFactory::create(std::string nick) {
  if(!instrumentated_){
    std::cout << "[CarFactory] create" << std::endl;
	return std::make_shared<Car>(nick);
  }

  auto car = std::make_shared<Tinman::InstrumentatedCarI>(nick);
  Ice::ObjectPrx peer_proxy = adapter_->add(car.get(),
				adapter_->getCommunicator()->stringToIdentity(nick));
  auto peer = Tinman::InstrumentatedCarPrx::checkedCast(peer_proxy->ice_twoway());
  std::cout << "[CarFactory]" << __func__ << ": " << nick << std::endl;
  std::cout << "\tproxy: " << peer << std::endl;
  return car;
}
