// -*- coding:utf-8; tab-width:4; mode:cpp -*-
// Copyright (C) 2014  ISAAC LACOBA MOLINA
// Tinman author: Isaac Lacoba Molina
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "timer.h"
#include <iostream>

Timer::Timer() {
  last_time_ = std::chrono::steady_clock::now();
  start_ = 0.f;
  stop_ = now();
}

Timer::~Timer() { }

float
Timer::get_delta_time() {
  delta_ = now() - last_time_;
  last_time_ = now();

  return time_to_float(delta_);
}

float
Timer::get_time_since_start() {
  start_ += get_delta_time();

  return truncate(start_);
}

void
Timer::start() {
  freeze_time_ = now() - stop_;
  start_ -= time_to_float(freeze_time_);
}

void
Timer::restart() {
  start_ = 0.f;
  stop_ = now();
  start();
}

void
Timer::stop() {
  stop_ = now();
}

float
Timer::truncate(float number) {
  return std::floor(number * 10) / 10;
}

Timer::Time
Timer::now() {
  return std::chrono::steady_clock::now();
}

float
Timer::time_to_float(Timer::DeltaTime time) {
  return std::chrono::duration_cast<std::chrono::duration<float>>(time).count();
}
