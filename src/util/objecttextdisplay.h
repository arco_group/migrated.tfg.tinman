//From: http://www.ogre3d.org/tikiwiki/ObjectTextDisplay
#include "scene.h"
#include "gui.h"

class ObjectTextDisplay {

 public:
  typedef std::shared_ptr<ObjectTextDisplay> shared;

  ObjectTextDisplay(const Ogre::MovableObject* p, const Ogre::Camera* c);
  virtual ~ObjectTextDisplay();
  void enable(bool enable);
  void setText(const Ogre::String& text);
  void update();

 protected:
  const Ogre::MovableObject* m_p;
  const Ogre::Camera* m_c;
  bool m_enabled;
  Ogre::Overlay* m_pOverlay;
  Ogre::OverlayElement* m_pText;
  Ogre::OverlayContainer* m_pContainer;
  Ogre::String m_text;
};
