// -*- coding:utf-8; tab-width:4; mode:cpp -*-
// Copyright (C) 2014  ISAAC LACOBA MOLINA
// Tinman author: Isaac Lacoba Molina
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "playerfactory.h"

Player::shared
HumanFactory::create(std::string nick, EventListener::shared input,
                     Sound::shared sound, Animation::shared animation, Car::shared car) {
  std::cout << "[HumanFactory] create" << std::endl;
  HumanController::KeyBinding player_bindings {
    {OIS::KC_W, {Tinman::Action::AccelerateBegin, Tinman::Action::AccelerateEnd}},
    {OIS::KC_S, {Tinman::Action::BrakeBegin,  Tinman::Action::BrakeEnd}},
    {OIS::KC_A, {Tinman::Action::TurnLeft,  Tinman::Action::TurnEnd}},
    {OIS::KC_D, {Tinman::Action::TurnRight,  Tinman::Action::TurnEnd}},
    {OIS::KC_SPACE, {Tinman::Action::UseNitro,  Tinman::Action::AccelerateEnd}}
  };

  Controller::shared human_controller =
    std::make_shared<HumanController>(input, player_bindings, car);
  return std::make_shared<Player>(nick, human_controller, sound, animation);
}

Player::shared
BotFactory::create(std::string nick, Physics::shared physics,
           Sound::shared sound, Animation::shared animation,
		   Race::shared race, Car::shared car){
  std::cout << "[BotFactory] create" << std::endl;
  BotController::shared bot = std::make_shared<BotController>(physics, car);
  std::dynamic_pointer_cast<BotController>(bot)->add_race(race);
  return std::make_shared<Player>(nick, bot, sound, animation);
}

Player::shared
NetworkFactory::create(std::string nick, Sound::shared sound,
		       Animation::shared animation, Car::shared car){
  std::cout << "[NetworkFactory] create" << std::endl;
  NetworkController::shared controller = std::make_shared<NetworkController>(car);
  return std::make_shared<Player>(nick, controller, sound, animation);
}

Player::shared
InstrumentatedFactory::create(std::string nick, Sound::shared sound,
                              Animation::shared animation, Car::shared car) {
  std::cout << "[InstrumentatedFactory] create" << std::endl;
  IController::shared controller = std::make_shared<IController>(car);
  return std::make_shared<Player>(nick, controller, sound, animation);
}
