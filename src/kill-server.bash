#!/bin/bash --
# -*- coding:utf-8; tab-width:4; mode:shell-script -*-
for process in $(ps fax | grep './network/server.py' | awk '{print $1}');
do
    kill -9 $process;
done
