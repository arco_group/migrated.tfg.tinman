// -*- coding:utf-8; tab-width:4; mode:cpp -*-
// Copyright (C) 2014  ISAAC LACOBA MOLINA
// Tinman author: Isaac Lacoba Molina
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "game.h"
const int INFINITE = 2147483647;

void
add_instrumentated_players(auto game, auto session, int n_players) {
  std::stringstream nickname;
  for (int i = 0; i < n_players; i++) {
    nickname << "iPlayer" << i;
    session->add_instrumentated_player(nickname.str());
    nickname.str("");
  }
}

void
add_bots(auto game, auto session, int n_bots) {
  std::stringstream nickname;
  for (int i = 0; i < n_bots; i++) {
    nickname << "Bot" << i;
    session->add_bot(nickname.str());
    nickname.str("");
  }
}


void
print_help() {
  std::cout << "Welcome to Tinman, the open source OffRoad game." <<
    "For more info, visit: https://bitbucket.org/arco_group/tfg.tinman" << std::endl;
  std::cout << "Usage:" << std::endl;
  std::cout << " --debugRender: render physics body bounding boxes" << std::endl;
  std::cout << " --players <number>: number of instrumentated players" <<
    " that will be controlled by a test" << std::endl;
    std::cout << " --bots <number>: number of bots that will be created. " << std::endl;
  std::cout << " --frames <number>: number of frames the game will be executed. " <<
    "If this argument is not present, game will be executed until" <<
    " player stop it." << std::endl;
  std::cout << " --track <path_to_track_file>: path to the track file" << std::endl;
}

int main(int argc, char *argv[]) {
  std::vector<std::string> args(argv + 1, argv + argc + !argc);
  Game::shared game;

  if(std::find(args.begin(), args.end(), "--help") != args.end()){
    print_help();
    exit(1);
  }

  auto debug_drawer = std::find(args.begin(), args.end(), "--debugRender");
  if(argc <= 2) {
    game =  std::make_shared<Game>();
    if(debug_drawer != args.end())
      game->add_debug_drawer();
    game->start();
    exit(1);
  }

  Session::shared session;
  Race::shared race;

  auto track = std::find(args.begin(), args.end(), "--track");
  if(track == args.end()) {
    std::cerr << "Error: no --track <path_to_track> found" << std::endl;
    exit(-1);
  }

  race = std::make_shared<Race>(*(++track));
  session = std::make_shared<Session>(race);

  game = std::make_shared<Game>(session);
  if(debug_drawer != args.end())
    game->add_debug_drawer();

  session->game_ = game;

  auto n_players = std::find(args.begin(), args.end(), "--players");
  if(n_players != args.end()) {
    add_instrumentated_players(game, session, atoi((++n_players)->c_str()));
  }

  auto n_bots = std::find(args.begin(), args.end(), "--bots");
  if(n_bots != args.end())
    add_bots(game, session, atoi((++n_bots)->c_str()));

  auto n_frames = std::find(args.begin(), args.end(), "--frames");
  if(n_frames != args.end())
    game->start(atoi((++n_frames)->c_str()));
  else
    game->start(INFINITE);

  return 0;
}
