// -*- coding:utf-8; tab-width:4; mode:cpp -*-
// Copyright (C) 2014  ISAAC LACOBA MOLINA
// Tinman author: Isaac Lacoba Molina
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "network.h"
#include <fstream>
#include "session.h"

std::string
get_config(std::string file_name) {
  std::ifstream file;
  file.open(file_name);

  std::string config;
  getline(file, config);
  return config;
}

ControllerObserver::ControllerObserver() {}

ControllerObserver::~ControllerObserver() {}

Adapter::Adapter(Car::shared car) {
  try {
    seq_number_ = 0;
    socket_ = {"1", 10000};
    car_ = car;
    Ice::StringSeq argv{"Tinman"};
    communicator_ = Ice::initialize(argv);
  }catch(const ::Ice::LocalException& exception) {
    std::cerr << "No server running" << std::endl;
  }
}

Adapter::~Adapter() {
}


Ice::AsyncResultPtr
Adapter::login(std::shared_ptr<Session> session) {
  try{
    if(peer_)
      return auth_server_->begin_login(session->human_player_->nick_, peer_);

    std::stringstream endpoint;
    endpoint << "tcp:udp";

    endpoint.str("");
    endpoint << "tinman: tcp -h " << socket_.first << " -p "<< socket_.second;
    endpoint << ": udp -h " << socket_.first << " -p "<< socket_.second + 1;
    endpoint.str();

    ObjectPrx proxy = communicator_->stringToProxy(endpoint.str());

    auth_server_ = Tinman::AuthoritativeServerPrx::checkedCast(proxy);
    dgram_auth_server_ = Tinman::AuthoritativeServerPrx::uncheckedCast(proxy->ice_datagram());


    Tinman::PeerPtr servant = new Tinman::PeerI(session);
    ObjectAdapterPtr adapter;
    adapter = communicator_->createObjectAdapterWithEndpoints("PeerAdapter", "tcp:udp");

    ObjectPrx peer_proxy = adapter->add(servant,
                                        communicator_->stringToIdentity("peer-tinman"));


    peer_ = Tinman::PeerPrx::uncheckedCast(peer_proxy->ice_datagram());
    std::cout << peer_proxy << std::endl;
    adapter->activate();

    return auth_server_->begin_login(session->human_player_->nick_, peer_);
  }catch(const ::IceUtil::NullHandleException& exception) {
    std::cerr << "Error: login failed!" << std::endl;
    return nullptr;
  }
  catch(const Ice::ConnectionRefusedException exception) {
    std::cerr << "Error: connection refused!" << std::endl;
    return nullptr;
  }

}

Tinman::LoginInfo
Adapter::complete_login(Ice::AsyncResultPtr async_result) {
  login_info_ = auth_server_->end_login(async_result);
  return login_info_;
}

Ice::AsyncResultPtr
Adapter::next_race(std::string nickname) {
  return auth_server_->begin_startRace(nickname);
}

void
Adapter::notify(Tinman::Action action) {
  Tinman::Vector3 position{car_->get_position().getX(),
      car_->get_position().getY(), car_->get_position().getZ()};

  Tinman::Vector3 velocity{car_->get_velocity().getX(),
      car_->get_velocity().getY(), car_->get_velocity().getZ()};

  Tinman::Quaternion orientation{car_->get_orientation().getX(),
      car_->get_orientation().getY(),
      car_->get_orientation().getZ(),
      car_->get_orientation().getW()};

  Tinman::CarInfo car_state;
  car_state.carId = login_info_.carId;
  car_state.seqNumber = ++seq_number_;
  car_state.orientation = orientation;
  car_state.position = position;
  car_state.velocity = velocity;
  car_state.actions = Tinman::ActionSeq{action};

  Tinman::Snapshot snapshot = {car_state};

  dgram_auth_server_->notify(snapshot);
}

void
Adapter::disconnect() {
  std::cout << "[Adapter] " << __func__ << std::endl;
  dgram_auth_server_->disconnect(login_info_.carId);
}
