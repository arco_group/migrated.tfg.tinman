#ifndef NETWORK_H
#define NETWORK_H
#include <Ice/Ice.h>
#include "tinmanI.h"
#include "car.h"

using namespace Ice;

class Session;

class ControllerObserver {
 protected:
  int seq_number_;
 public:
  typedef std::shared_ptr<ControllerObserver> shared;
  std::pair<std::string, int> socket_;

  ControllerObserver();
  virtual ~ControllerObserver();

  virtual void notify(Tinman::Action action) = 0;
  virtual Ice::AsyncResultPtr  login(std::shared_ptr<Session> session) = 0;
  virtual Tinman::LoginInfo complete_login(Ice::AsyncResultPtr async_result) = 0;
  virtual Ice::AsyncResultPtr next_race(std::string nickname) = 0;
  virtual void disconnect() = 0;
};

class Adapter: public ControllerObserver {
  Ice::CommunicatorPtr communicator_;
  Tinman::LoginInfo login_info_;
  Tinman::AuthoritativeServerPrx dgram_auth_server_, auth_server_;
  Car::shared car_;
  Tinman::PeerPrx peer_;

 public:
  typedef std::shared_ptr<Adapter> shared;

  Adapter(Car::shared car);
  virtual ~Adapter();

  void notify(Tinman::Action action);
  Ice::AsyncResultPtr login(std::shared_ptr<Session> session);
  Tinman::LoginInfo complete_login(Ice::AsyncResultPtr async_result);
  Ice::AsyncResultPtr next_race(std::string nickname);
  void notify(Tinman::Snapshot snapshot);
  void disconnect();

 private:
  std::string get_endpoint();
};
#endif
