#include <Ice/BuiltinSequences.ice>

module Tinman {
  enum Action {AccelerateBegin, AccelerateEnd,
               BrakeBegin, BrakeEnd,
               TurnRight, TurnLeft, TurnEnd,
               UseNitro};
  sequence<Action> ActionSeq;

  struct Quaternion {
    float x;
    float y;
    float z;
    float w;
  };

  struct Vector3 {
    float x;
    float y;
    float z;
  };

  struct CarInfo {
    byte carId;
    int seqNumber;
    Quaternion orientation;
    Vector3 position;
    Vector3 velocity;
    ActionSeq actions;
  };

  sequence<CarInfo> Snapshot;

  struct LoginInfo {
    byte carId;
    Ice::StringSeq playerNicks;
  };

  interface Peer {
    void notify(Snapshot value);
  };

  interface AuthoritativeServer {
    void notify(Snapshot valuey);
    ["amd"] LoginInfo login(string nickname, Peer* proxy);
    ["amd"] void startRace(string nickname);
    void disconnect(byte id);
  };

  interface InstrumentatedCar {
    void exec(Tinman::Action action);

    int getNitro();
    int getLap();
    Vector3 getPosition();
    Vector3 getVelocity();
    Quaternion getOrientation();
    Vector3 getDirection(int factor);
    float getSpeed();
    bool isColliding();
    bool isStuck();
    void move(Vector3 position);
  };
};
