#ifndef __instrumentatedCarI_h__
#define __instrumentatedCarI_h__

#include <tinman.h>

#include "car.h"


namespace Tinman
{

  class InstrumentatedCarI : virtual public InstrumentatedCar, public Car {
  public:
    typedef std::shared_ptr<InstrumentatedCarI> shared;

    InstrumentatedCarI(std::string nick);

    virtual void exec(::Tinman::Action,
                      const Ice::Current&);

    virtual ::Ice::Int getNitro(const Ice::Current&);

    virtual ::Ice::Int getLap(const Ice::Current&);

    virtual ::Tinman::Vector3 getPosition(const Ice::Current&);

    virtual ::Tinman::Vector3 getVelocity(const Ice::Current&);

    virtual ::Tinman::Quaternion getOrientation(const Ice::Current&);

    virtual ::Tinman::Vector3 getDirection(::Ice::Int,
                                           const Ice::Current&);

    virtual ::Ice::Float getSpeed(const Ice::Current&);

    virtual bool isColliding(const Ice::Current&);
    virtual bool isStuck(const Ice::Current&);

    virtual void move(const ::Tinman::Vector3&, const Ice::Current&);
  };

}

#endif
