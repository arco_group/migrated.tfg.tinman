#include <tinmanI.h>
#include "session.h"

Tinman::PeerI::PeerI(Session::shared session) {
  session_ = session;
}

void
Tinman::PeerI::notify(const ::Tinman::Snapshot& snapshot,
                      const Ice::Current& current) {
  session_->store(snapshot);
}
