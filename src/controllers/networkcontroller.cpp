// -*- coding:utf-8; tab-width:4; mode:cpp -*-
#include "networkcontroller.h"

NetworkController::NetworkController(Car::shared car):  Controller(car) {
  duration_ = last_size_ = 0.f;
  last_num_sequence_ = -1;
}

NetworkController::~NetworkController() {
}

void
NetworkController::configure() {
}

void
NetworkController::exec(Tinman::Action action) {
    car_->exec(action);
}


void
NetworkController::update(float delta) {
  for(int snapshots_per_frame = get_snapshots_per_frame(delta);
      snapshots_per_frame != 0;
      snapshots_per_frame--) {

    if(updates_.size() > 1 &&
       updates_.front().seqNumber > last_num_sequence_) {

      for(auto action: updates_.front().actions)
        car_->exec(action);
      car_->synchronize(updates_.front());
      last_num_sequence_ = updates_.front().seqNumber;
      updates_.pop();
    }
  }

  car_->update(delta);
}

int
NetworkController::get_snapshots_per_frame(float delta) {
  if(updates_.empty()) {
    int snapshots_initial_case = 0;
    return snapshots_initial_case;
  }

  if(updates_.size() <= 6) {
    return 1;
  }

  int snapshots_per_frame = updates_.size() - 6;
  return snapshots_per_frame;
}
