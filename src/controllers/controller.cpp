// -*- coding:utf-8; tab-width:4; mode:cpp -*-
#include "controller.h"

using namespace Tinman;

Controller::Controller(Car::shared car) {
  car_ = car;
  observer_ = std::make_shared<Adapter>(car_);
}

Controller::~Controller() {}

void
Controller::disconnect()  {
  observer_->disconnect();
}

void
Controller::store(Tinman::CarInfo car_info) {
  updates_.push(std::move(car_info));
}
