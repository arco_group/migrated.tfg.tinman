// Copyright (C) 2014  ISAAC LACOBA MOLINA
// Tinman author: Isaac Lacoba Molina
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef BOT_H
#define BOT_H
#include <cmath>


#include "controller.h"
#include "physics.h"

class Race;

class BotController: public Controller {
  int index_;
  std::shared_ptr<Race> race_;
  Physics::shared physics_;
  Section previous_segment_, current_segment_;
  float stucked_, colliding_;
  const float collide_limit_ = 1.f;
  const float stuck_limit_ = 1.f;
  bool stuck_, collide_;

public:
  BotController(Physics::shared physics, Car::shared car);
  virtual ~BotController();
  void update(float delta);
  void exec(Tinman::Action action);
  void configure();

  void add_race(std::shared_ptr<Race> race);

 private:
  void perform_action(btVector3 next_destination);
  void go_to_waypoint();
  bool detect_obstacles(float delta);
};
#endif
