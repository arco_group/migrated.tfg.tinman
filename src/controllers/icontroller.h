// Copyright (C) 2014  ISAAC LACOBA MOLINA
// Tinman author: Isaac Lacoba Molina
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef ICONTROLLER_H
#define ICONTROLLER_H
#include "controller.h"
#include "instrumentatedCarI.h"

class IController: public Controller {

public:
  typedef std::shared_ptr<IController> shared;
  IController(Car::shared car);
  virtual ~IController();

  void update(float delta);
  void exec(Tinman::Action action);
  void configure();
};

#endif
