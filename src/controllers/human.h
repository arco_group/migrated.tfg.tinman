// Copyright (C) 2014  ISAAC LACOBA MOLINA
// Tinman author: Isaac Lacoba Molina
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef HUMANCONTROLLER_H
#define HUMANCONTROLLER_H
#include "controller.h"
#include "input.h"

class HumanController: public Controller {
  EventListener::shared input_;
 public:
  typedef std::map<OIS::KeyCode,
    std::pair<Tinman::Action, Tinman::Action>> KeyBinding;
  bool network_mode_;

  HumanController(EventListener::shared listener,
                  KeyBinding key_bindings, Car::shared car);
  virtual ~HumanController();

  void configure();
  void exec(Tinman::Action action);
  void update(float delta);

private:
  KeyBinding key_bindings_;
};

#endif
