#ifndef PAUSE_H
#define PAUSE_H
#include "state.h"

class Pause: public State {
  CEGUI::Window* menu_window_;
  bool menu_visible_, is_init_;

public:
  typedef std::shared_ptr<Pause> shared;
  Pause(std::shared_ptr<Game> game);
  virtual ~Pause();

  void enter();
  void update(float delta);
  void switch_menu();

 private:
  bool change_state(const CEGUI::EventArgs &event);
  bool gui_shutdown(const CEGUI::EventArgs &event);
  void add_hooks();
  void add_gui_hooks();
  void add_hooks(const std::string& button,
                 const CEGUI::Event::Subscriber& callback);
};
#endif
