// -*- coding:utf-8; tab-width:4; mode:cpp -*-
#include "statemachine.h"

StateMachine::StateMachine(Game::shared game) {
  game_ = game;

  state_table_[StateName::Menu] = std::make_shared<Menu>(game);
  state_table_[StateName::Play] = std::make_shared<Play>(game);
  state_table_[StateName::Pause] = std::make_shared<Pause>(game);
  state_table_[StateName::Results] = std::make_shared<Results>(game);
}

StateMachine::~StateMachine() {

}

void
StateMachine::change_state(void) {
  std::cout << "[StateMachine] change_state" << std::endl;
  if(current_state_ == StateName::Initial) {
    set_current_state(StateName::Menu);
    return;
  }

  if(current_state_ ==  StateName::Menu) {
    Menu::shared menu =  std::dynamic_pointer_cast<Menu>(state_table_[StateName::Menu]);
    menu->hide_waiting_screen();
    menu->expose_track_->reset();
    if(game_->mode_ == Game::Mode::Bots) {
      bool network_mode = false;
      std::dynamic_pointer_cast<HumanController>(
              game_->session_->human_player_->controller_)->network_mode_ = network_mode;

      std::stringstream nick;
      for (int i = 0; i < 3; ++i){
        nick << "bot" << i;
        game_->session_->add_bot(nick.str());
        nick.str("");
      }
      game_->session_->players_nicks_ = {game_->session_->human_player_->get_nick(), };
    }

    game_->session_->realize();
    set_current_state(StateName::Play);
    return;
  }

  if(current_state_ ==  StateName::Play) {
    if(game_->session_->race_->has_ended()){
      set_current_state(StateName::Results);
    }
    else{
      game_->session_->race_->pause();
      set_current_state(StateName::Pause);
    }
    return;
  }

  if(current_state_ == StateName::Pause) {
    Pause::shared pause = std::dynamic_pointer_cast<Pause>(state_table_[StateName::Pause]);
    pause->switch_menu();
    set_current_state(StateName::Play);
    return;
  }

  if(current_state_ ==  StateName::Results) {
    game_->session_->reset();
    set_current_state(StateName::Play);
  }
}

void
StateMachine::set_current_state(StateName next_state) {
  current_state_ = next_state;
  game_->input_->clear_hooks();
  state_table_[current_state_]->enter();
}

State::shared
StateMachine::get_state(StateName state) {
  return state_table_[state];
}

State::shared
StateMachine::get_current_state() {
  return state_table_[current_state_];
}
