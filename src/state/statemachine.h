#ifndef STATEMACHINE_H
#define STATEMACHINE_H

#include "game.h"

class StateMachine {
  typedef std::map<StateName, State::shared> StateTable;
  StateTable state_table_;
  StateName current_state_;
  Game::shared game_;

public:
  typedef std::shared_ptr<StateMachine> shared;

  StateMachine(Game::shared game);
  virtual ~StateMachine();

  void change_state(void);
  void set_current_state(StateName next_state);


  State::shared get_state(StateName state);
  State::shared get_current_state();
};
#endif
