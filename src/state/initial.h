// -*- coding:utf-8; tab-width:4; mode:cpp -*-
// Tinman author: Isaac Lacoba Molina
//   Copyright (C) 2014  ISAAC LACOBA MOLINA
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef INITIAL_H
#define INITIAL_H
#include "state.h"


class Initial: public State {
  CEGUI::Window* menu_window_;

 public:
  typedef std::shared_ptr<Initial> shared;

  Initial(std::shared_ptr<Game> game);
  virtual ~Initial();

  void enter();
  void update(float delta);

 private:
  bool change_state(const CEGUI::EventArgs &event);

  void add_hooks();
  void add_hooks(const std::string& button,
                 const CEGUI::Event::Subscriber& callback);
  void add_gui_hooks();
  void init_gui();
  void load_window();
};

#endif
