// -*- coding:utf-8; tab-width:4; mode:cpp -*-
#include "game.h"

Pause::Pause(std::shared_ptr<Game> game): State(game) {
  menu_visible_ = is_init_ = false;
  menu_window_ = game_->gui_->load_layout("Pause.layout");
  menu_window_->hide();
}

Pause::~Pause() {
  delete menu_window_;
}

void
Pause::enter() {
  game_->sound_->pause();
  add_hooks();
  switch_menu();
  CEGUI::GUIContext& context_ = CEGUI::System::getSingleton().getDefaultGUIContext();
  context_.setRootWindow(menu_window_);

  if(is_init_)
    return;
  add_gui_hooks();
  is_init_ = true;
}

bool
Pause::change_state(const CEGUI::EventArgs &event) {
  game_->change_state();
  return true;
}

void
Pause::update(float delta) {
}

void
Pause::add_hooks() {
  game_->input_->add_hook({OIS::KC_ESCAPE, EventTrigger::OnKeyPressed}, EventType::doItOnce,
                          std::bind(&Game::change_state, game_.get()));
}

void
Pause::add_gui_hooks() {
  add_hooks("Exit", CEGUI::Event::Subscriber(&Pause::gui_shutdown,
                                             this));
  add_hooks("Resume", CEGUI::Event::Subscriber(&Pause::change_state, this));
}

void
Pause::add_hooks(const std::string& button,
                 const CEGUI::Event::Subscriber& callback) {
  menu_window_->getChild(button)->
    subscribeEvent(CEGUI::PushButton::EventClicked, callback);
}

void
Pause::switch_menu() {
  menu_visible_ = !menu_visible_;
  menu_window_->setVisible(menu_visible_);
}

bool
Pause::gui_shutdown(const CEGUI::EventArgs &event) {
  game_->disconnect();
  game_->shutdown();
  return true;
}
