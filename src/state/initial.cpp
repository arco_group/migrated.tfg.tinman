// -*- coding:utf-8; tab-width:4; mode:cpp -*-
// Tinman author: Isaac Lacoba Molina
//   Copyright (C) 2014  ISAAC LACOBA MOLINA
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "game.h"


Initial::Initial(std::shared_ptr<Game> game): State(game) {
}

Initial::~Initial() {
  delete menu_window_;
}

void
Initial::enter() {
  init_gui();
  add_hooks();
  add_gui_hooks();
}

void
Initial::init_gui() {
  load_window();
}

void
Initial::load_window() {
  menu_window_ = game_->gui_->load_layout("Initial.layout");
  CEGUI::GUIContext& context_ = CEGUI::System::getSingleton().getDefaultGUIContext();
  context_.setRootWindow(menu_window_);
}

bool
Initial::change_state(const CEGUI::EventArgs &event) {
  game_->input_->clear_hooks();
  menu_window_->hide();
  game_->set_current_state(StateName::Menu);
  return true;
}

void
Initial::update(float delta) {
  game_->gui_->inject_delta(game_->delta_);
}

void
Initial::add_hooks() {
  game_->input_->add_hook({OIS::KC_ESCAPE, EventTrigger::OnKeyPressed}, EventType::doItOnce,
                          std::bind(&Game::shutdown, game_.get()));
}

void
Initial::add_gui_hooks() {
  add_hooks("Start", CEGUI::Event::Subscriber(&Initial::change_state, this));
}

void
Initial::add_hooks(const std::string& button,
                 const CEGUI::Event::Subscriber& callback) {
  menu_window_->getChild(button)->
    subscribeEvent(CEGUI::PushButton::EventClicked, callback);
}
