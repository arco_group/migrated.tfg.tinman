// -*- coding:utf-8; tab-width:4; mode:cpp -*-
#include "game.h"

Results::Results(std::shared_ptr<Game> game): State(game) {
  is_init_ = false;
  menu_window_ = game_->gui_->load_layout("Results.layout");
  shop_window_ = game_->gui_->load_layout("Shop.layout");
  waiting_window_ = game_->gui_->load_layout("WaitingNextRace.layout");
  shop_window_->hide();
  waiting_window_->hide();
  add_gui_hooks();
  last_nitro_ = 0.f;
}

Results::~Results() {
  delete menu_window_;
}

void
Results::enter() {
  std::cout << "[Result] enter" << std::endl;
  add_hooks();
  show_results();
}

void
Results::show_results() {
  CEGUI::GUIContext& context_ = CEGUI::System::getSingleton().getDefaultGUIContext();
  context_.setRootWindow(menu_window_);
  std::stringstream result;
  int i = 0;

  std::sort(game_->race_results_.begin(), game_->race_results_.end(),
            [](auto player1, auto player2) {
              return player1.second > player2.second;
            });

  for (auto results: game_->race_results_) {
    std::cout << "uuid: " << results.first
              << "position: " << results.second << std::endl;
    result << ++i << "Result";
    menu_window_->getChild(result.str())->
      setText(results.first);
    result.str("");
  }
  menu_window_->setVisible(true);
}

void
Results::add_hooks() {
  game_->input_->add_hook({OIS::KC_ESCAPE, EventTrigger::OnKeyPressed}, EventType::doItOnce,
                          std::bind(&Game::shutdown, game_.get()));
}

void
Results::add_gui_hooks() {
  add_hooks(menu_window_, "ToShop", CEGUI::Event::Subscriber(&Results::show_shop, this));
  add_hooks(shop_window_, "NextRace", CEGUI::Event::Subscriber(&Results::change_state, this));
  add_hooks(shop_window_, "NitroBtn", CEGUI::Event::Subscriber(&Results::sell_nitro, this));
}

void
Results::add_hooks(CEGUI::Window* window, const std::string& button,
                 const CEGUI::Event::Subscriber& callback) {
  window->getChild(button)->
    subscribeEvent(CEGUI::PushButton::EventClicked, callback);
}

bool
Results::change_state(const CEGUI::EventArgs &event) {
  if(game_->mode_ == Game::Mode::Bots) {
    menu_window_->hide();
    waiting_window_->hide();
    shop_window_->hide();
    game_->change_state();
    return true;
  }

  waiting_window_->setVisible(true);
  CEGUI::GUIContext& context_ = CEGUI::System::getSingleton().getDefaultGUIContext();
  context_.setRootWindow(waiting_window_);
  next_race_ = game_->get_human_player()->next_race();
  return true;
}

void
Results::update(float delta) {
  if(next_race_ && next_race_->isCompleted()) {
    menu_window_->hide();
    waiting_window_->hide();
    shop_window_->hide();
    game_->change_state();
    next_race_ = nullptr;
  }
}

bool
Results::show_shop(const CEGUI::EventArgs &event) {
  menu_window_->hide();
  shop_window_->setVisible(true);
  CEGUI::GUIContext& context_ = CEGUI::System::getSingleton().getDefaultGUIContext();
  context_.setRootWindow(shop_window_);
  config_progress_bar();
  shop_window_->getChild("Money")->
    setText(std::to_string(game_->get_human_player()->money_));
  return true;
}

void
Results::config_progress_bar() {
  CEGUI::ProgressBar* progress =
    static_cast<CEGUI::ProgressBar*>(shop_window_->getChild("NitroBtn")->
                                     getChild("Nitro"));
  progress->setStepSize(0.10f);
  int nitro_state = game_->get_human_player()->get_car()->controller_->nitro_ / 100;
  progress->adjustProgress(nitro_state - last_nitro_);
  last_nitro_ = nitro_state;
}

bool
Results::sell_nitro(const CEGUI::EventArgs &event) {
  if(game_->get_human_player()->money_ < 10000)
    return false;

  CEGUI::ProgressBar* progress =
    static_cast<CEGUI::ProgressBar*>(
        shop_window_->getChild("NitroBtn")->getChild("Nitro"));
  progress->step();
  game_->get_human_player()->purchase();

  shop_window_->getChild("Money")->
    setText(std::to_string(game_->get_human_player()->money_));
  return true;
}
