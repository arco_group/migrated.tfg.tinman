// Copyright (C) 2014  ISAAC LACOBA MOLINA
// Tinman author: Isaac Lacoba Molina
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef GUI_H
#define GUI_H
#include <memory>

#include <OgreOverlayContainer.h>
#include <OgreOverlayManager.h>
#include <OgreOverlayElement.h>
#include <OgreTextAreaOverlayElement.h>
#include <OgreFontManager.h>

#include <CEGUI/CEGUI.h>
#include <CEGUI/RendererModules/Ogre/Renderer.h>

class GUI {
  Ogre::OverlayManager* overlay_manager_;

  const std::string default_font_ = "Manila-12";

public:
  typedef std::shared_ptr<GUI> shared;

  GUI();
  virtual ~GUI();
  void switch_menu();
  void register_callback(std::string button, CEGUI::Event::Subscriber callback);

  CEGUI::Window* load_layout(std::string file);
  CEGUI::AnimationInstance* load_animation(std::string name, CEGUI::Window* window);
  Ogre::OverlayElement* create_overlay(std::string name, std::string element);
  Ogre::OverlayElement* create_overlay(std::string name, std::string element,
                                       std::pair<float, float> position,
                                       std::pair<float, float> dimension);
  CEGUI::GUIContext& get_context();
  void inject_delta(float delta);

private:
  void load_resources();
  void init();
};


#endif
