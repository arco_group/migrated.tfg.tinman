// Tinman author: Isaac Lacoba Molina
// Copyright (C) 2014  ISAAC LACOBA MOLINA
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
#ifndef SCENE_H
#define SCENE_H
#include <OgreRoot.h>
#include <OgreLogManager.h>
#include <OgreRenderWindow.h>
#include <OgreWindowEventUtilities.h>
#include <OgreCamera.h>
#include <OgreSceneManager.h>
#include <OgreResourceGroupManager.h>
#include <OgreConfigFile.h>
#include <OgreManualObject.h>
#include <OgreEntity.h>
#include <OgreMeshManager.h>
#include <OgreParticleSystem.h>

#include <LinearMath/btVector3.h>

#include <memory>

class Scene {
  const std::string window_title = "Tinman Project";

  Ogre::Root* root_;
  Ogre::SceneManager* scene_manager_;

 public:
  typedef std::shared_ptr<Scene> shared;

  Ogre::RenderWindow* window_;
  Ogre::Camera* camera_;
  Ogre::RaySceneQuery* ray_query_;

  Scene();
  void render_one_frame(void);
  Ogre::Ray set_ray_query(float x, float y);
  void create_light(void);
  void set_light_range( Ogre::Light *light, Ogre::Real range);

  void create_camera(Ogre::RenderWindow* window);

  Ogre::SceneNode* get_node(std::string node);
  void attach(Ogre::SceneNode* node, Ogre::Entity* entity);

  Ogre::SceneNode* create_graphic_element(Ogre::Vector3 position,
                                          std::string name,
                                          std::string mesh);
  Ogre::SceneNode* create_graphic_element(std::string entity,
        std::string mesh, std::string parent, std::string name);
  Ogre::SceneNode* create_graphic_element(Ogre::Entity* entity,
        std::string parent, std::string name);

  Ogre::SceneNode* create_plane(std::string axis, std::string name, std::string mesh,
        std::string parent, std::string material);

  Ogre::SceneNode* get_child_node(std::string parent, std::string name);
  Ogre::SceneNode* get_child_node(Ogre::SceneNode* parent, std::string name);

  Ogre::Entity* create_entity(std::string name, std::string mesh, bool cast_shadows);

  void move_node(std::string node_name, Ogre::Vector3 increment);

  Ogre::ParticleSystem* get_particle(std::string name, std::string particle_system);
  Ogre::ParticleSystem* get_particle(Ogre::SceneNode* node, std::string name, std::string particle_system);
  Ogre::ParticleSystem* get_particle(std::string name, std::string particle_system, Ogre::Vector3 position);

  void add_child(Ogre::SceneNode* parent, Ogre::SceneNode* child);
  void destroy_node(std::string);
  void destroy_node(Ogre::SceneNode* child);
  void destroy_entity(Ogre::Entity* entity);

  void remove_child(std::string parent, std::string child);
  void remove_child(std::string parent, Ogre::SceneNode* child);
  void remove_child(Ogre::SceneNode* parent, std::string child);
  void remove_child(Ogre::SceneNode* parent, Ogre::SceneNode* child);

  void destroy_scene();
  void destroy_all_attached_movable_objects(Ogre::SceneNode* node);

  Ogre::Vector3 convert_btvector3_to_vector3(btVector3 position);

 private:
  void load_resources();
  Ogre::SceneNode* create_node(std::string name);
  Ogre::SceneNode* create_child_node(Ogre::SceneNode* parent, std::string name);

  Ogre::Vector3 get_axis(std::string axis);
  Ogre::Vector3 get_normal(std::string axis);
};
#endif
