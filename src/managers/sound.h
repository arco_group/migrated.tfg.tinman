#ifndef _SOUND_H
#define	_SOUND_H

#include <SDL/SDL.h>
#include <SDL/SDL_mixer.h>
#include <string>
#include <iostream>
#include <memory>
#include <unistd.h>

class Sound {
 public:
  typedef std::shared_ptr<Sound> shared;
  Sound();
  ~Sound();

  void play(const std::string& fileName) const;
  void pause();
  void stop();

 private:
  void init_audio_device();

  bool is_paused() const;
  bool is_stopped() const;
  bool is_playing() const;
  bool in_error_state() const;
};

#endif
