// -*- coding:utf-8; tab-width:4; mode:cpp -*-
// Copyright (C) 2014  ISAAC LACOBA MOLINA
// Tinman author: Isaac Lacoba Molina
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "sound.h"

Sound::Sound() {
  init_audio_device();
}

Sound::~Sound() {
  Mix_CloseAudio();
}

void
Sound::pause() {
  if (!is_playing())
    return;

  if (is_paused())
    Mix_ResumeMusic();
  else
    Mix_PauseMusic();

}

void
Sound::stop() {
  Mix_HaltMusic();
}

void
Sound::play(const std::string& file_name) const {
  Mix_Chunk* fx = Mix_LoadWAV(file_name.c_str());
  Mix_PlayChannel(-1, fx, 0);
  // Mix_VolumeChunk(fx, MIX_MAX_VOLUME);
}

void
Sound::init_audio_device() {
  SDL_Init(SDL_INIT_AUDIO);
  Mix_OpenAudio(MIX_DEFAULT_FREQUENCY, MIX_DEFAULT_FORMAT,
                MIX_DEFAULT_CHANNELS, 4096);
}

bool
Sound::is_paused() const {
  return Mix_PausedMusic() == 1;
}

bool
Sound::is_stopped() const {
  return !is_playing();
}

bool
Sound::is_playing() const {
  return Mix_PlayingMusic() == 1;
}
