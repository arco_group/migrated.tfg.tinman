// -*- coding:utf-8; tab-width:4; mode:cpp -*-
// Copyright (C) 2014  ISAAC LACOBA MOLINA
// Tinman author: Isaac Lacoba Molina
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "physics.h"

Physics::Physics() {
    btVector3 world_min(-1000,-1000,-1000);
    btVector3 world_max(1000,1000,1000);

    broadphase_ = new btAxisSweep3(world_min,world_max);

    solver_ = new btSequentialImpulseConstraintSolver();
    collision_configuration_ = new btDefaultCollisionConfiguration();
    dispatcher_ = new btCollisionDispatcher(collision_configuration_);

    dynamics_world_ = new btDiscreteDynamicsWorld(dispatcher_,
                        broadphase_, solver_, collision_configuration_);
    dynamics_world_->setGravity(gravity_);
  }



Physics::~Physics() {
  delete dynamics_world_;
  delete dispatcher_;
  delete collision_configuration_;
  delete solver_;
  delete broadphase_;
}

btRigidBody*
Physics::create_rigid_body(const btTransform &world_transform,
                           Ogre::SceneNode* node,
                           btCollisionShape* shape,
                           btScalar mass){
  btVector3 inertia(0 ,0 ,0);

  if(mass != 0)  shape->calculateLocalInertia(mass, inertia);

  MotionState* motionState = new MotionState(world_transform, node);
  btRigidBody::btRigidBodyConstructionInfo
    rigidBodyCI(mass, motionState, shape, inertia);

  btRigidBody* rigidBody = new btRigidBody(rigidBodyCI);

  add_rigid_body(rigidBody);

  return rigidBody;
}


btRigidBody*
Physics::create_rigid_body(const btTransform &world_transform,
                           btCollisionShape* shape,
                           btScalar mass) {
  btVector3 inertia(0 ,0 ,0);

  if(mass != 0)  shape->calculateLocalInertia(mass, inertia);

  btDefaultMotionState* motionState =
    new btDefaultMotionState(world_transform);
  btRigidBody::btRigidBodyConstructionInfo
    rigidBodyCI(mass, motionState, shape, inertia);

  btRigidBody* rigidBody = new btRigidBody(rigidBodyCI);
  add_rigid_body(rigidBody);

  return rigidBody;
}


void
Physics::add_rigid_body(btRigidBody* body) {
  dynamics_world_->addRigidBody(body);
}

void
Physics::remove_rigid_body(btRigidBody* body) {
  dynamics_world_->removeRigidBody(body);
}

btCollisionShape*
Physics::create_shape(btVector3 halfExtent){
  return new btBoxShape(halfExtent);
}

btCollisionShape*
Physics::create_shape(float radius){
  return new btSphereShape(radius);
}

btCollisionShape*
Physics::create_shape(MeshStrider* strider){
  return new btBvhTriangleMeshShape(strider, true, true);
}

btCollisionShape*
Physics::create_shape(btVector3 coordinates,
                      btScalar distance_to_origin) {
  return new btStaticPlaneShape(coordinates, distance_to_origin);
}

btCompoundShape*
Physics::create_compound_shape(btVector3 origin, btCollisionShape* child){
  btCompoundShape* compound = new btCompoundShape();
  btTransform localTrans;
  localTrans.setIdentity();
  localTrans.setOrigin(origin);

  compound->addChildShape(localTrans, child);
  return compound;
}


void
Physics::step_simulation(float deltaT, int maxSubSteps) {
  dynamics_world_->stepSimulation(deltaT, maxSubSteps);
}


void
Physics::check_collision() {
  int contact_point_caches = dynamics_world_->getDispatcher()->getNumManifolds();

    for (int i=0;i < contact_point_caches;i++) {
      btPersistentManifold* contact_cache =
        dynamics_world_->getDispatcher()->getManifoldByIndexInternal(i);

      const btCollisionObject* object_a = contact_cache->getBody0();
      const btCollisionObject* object_b = contact_cache->getBody1();

      if(triggers_[Physics::CollisionPair{object_a, object_b}]) {
        triggers_[Physics::CollisionPair{object_a, object_b}]();
      }
      if (triggers_[Physics::CollisionPair{object_b, object_a}]) {
        triggers_[Physics::CollisionPair{object_b, object_a}]();
      }
    }
}


void
Physics::add_collision_hooks(Physics::CollisionPair collision_pair,
			     std::function<void()> callback) {
  if(!triggers_[collision_pair]) {
    triggers_[collision_pair] = callback;
  }
}

void
Physics::clear_triggers() {
  triggers_.clear();
}

void
Physics::set_position(btRigidBody* body, btVector3 new_position) {
  btTransform transform = body->getCenterOfMassTransform();
  transform.setOrigin(new_position);
  body->setCenterOfMassTransform(transform);
}

void
Physics::set_transform(btRigidBody* body, btTransform transform) {
 body->setCenterOfMassTransform(transform);
}

btCollisionWorld::ClosestRayResultCallback
Physics::raytest(btVector3 position, btVector3 next_destination) {

  btCollisionWorld::ClosestRayResultCallback raycallback(position, next_destination);
  dynamics_world_->rayTest(position, next_destination, raycallback);
  return raycallback;
}

void
Physics::print_vector(std::string message, btVector3 vector3) {
  std::cout << message << " x: " <<        vector3.x()
              << "y: " <<                  vector3.y()
              << "z: " <<                  vector3.z()
              << std::endl;
}

btVector3
Physics::rotate_vector(btVector3 vector, btVector3 axis, btScalar angle) {
  return vector.rotate(axis, angle).normalize();
}

btScalar
Physics::get_angle(btVector3 origin, btVector3 destiny) {
  btVector3 direction = (origin - destiny);

  float angle = std::atan(direction.z() / direction.x());
  angle = direction.x() < 0 ? angle + (PI/2) : angle;
  return angle;
}

float
radians_to_angle(float radians) {
  return radians * 57.29577951;
}

float
degree_to_radians(float degrees) {
  return degrees * 0.0174532925;
}
