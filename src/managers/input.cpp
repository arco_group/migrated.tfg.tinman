// -*- coding:utf-8; tab-width:4; mode:cpp -*-
// Copyright (C) 2014  ISAAC LACOBA MOLINA
// Tinman author: Isaac Lacoba Molina
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

#include "input.h"

EventListener::EventListener(Ogre::RenderWindow* window) {
  x = y = 0;
  exit_ = false;
  mouse_key_.first = OIS::MB_Button7;

  create_input_manager(window);

  keyboard_ = static_cast<OIS::Keyboard*>
    (input_manager_->createInputObject(OIS::OISKeyboard, true));
  mouse_ = static_cast<OIS::Mouse*>
    (input_manager_->createInputObject(OIS::OISMouse, true));
   OIS::MouseState &mutableMouseState = const_cast<OIS::MouseState &>(mouse_->getMouseState());
   mutableMouseState.X.abs = window->getViewport(0)->getActualWidth() / 2;
   mutableMouseState.Y.abs = window->getViewport(0)->getActualHeight() / 2;

  keyboard_->setEventCallback(this);
  mouse_->setEventCallback(this);
  Ogre::WindowEventUtilities::addWindowEventListener(window, this);

}

void
EventListener::add_hook(EventListener::KeyBoardKey keystroke,
                        EventType type, std::function<void()> callback) {

  if(type == EventType::repeat
     && !repeat_triggers_[keystroke])
      repeat_triggers_[keystroke] = callback;
  else if(type == EventType::doItOnce
     && !doitonce_triggers_[keystroke]) {
      doitonce_triggers_[keystroke] = callback;
  }
}

void
EventListener::add_hook(MouseKey key,  std::function<void()> callback) {
    if(!mouse_triggers_[key])
      mouse_triggers_[key] = callback;
}

void
EventListener::capture(void) {
    keyboard_->capture();
    mouse_->capture();
}

void
EventListener::check_events(void) {
  if(mouse_key_.first != OIS::MB_Button7)
    trigger_mouse_events();

  if(!events_.empty())
    trigger_keyboard_events();
}

void
EventListener::trigger_mouse_events() {
  if(mouse_triggers_[mouse_key_]) {
    mouse_triggers_[mouse_key_]();
    mouse_key_ =  {OIS::MB_Button7, EventTrigger::OnKeyReleased};
  }
}

void
EventListener::trigger_keyboard_events() {
  if(events_.size() > doitonce_triggers_.size() + repeat_triggers_.size())
    return;

  for(auto event: events_){
    if(doitonce_triggers_[event]){
      doitonce_triggers_[event]();
      remove_key_from_buffer(event);
    }

    if(repeat_triggers_[event]) {
      repeat_triggers_[event]();
    }
  }
}

bool
EventListener::shutdown(void) {
    exit_ = true;
    return true;
}

void
EventListener::clear_hooks() {
  mouse_triggers_.clear();
  repeat_triggers_.clear();
  doitonce_triggers_.clear();
}

bool
EventListener::keyPressed(const OIS::KeyEvent& arg) {
  CEGUI::GUIContext& context = CEGUI::System::getSingleton().
    getDefaultGUIContext();
  context.injectKeyDown((CEGUI::Key::Scan) arg.key);
  context.injectChar(arg.text);

  remove_key_from_buffer({arg.key, EventTrigger::OnKeyReleased});
  events_.push_back({arg.key, EventTrigger::OnKeyPressed});
  return true;
}

bool
EventListener::keyReleased(const OIS::KeyEvent& arg) {
  CEGUI::GUIContext& context = CEGUI::System::getSingleton().
    getDefaultGUIContext();
  context.injectKeyUp((CEGUI::Key::Scan)arg.key);

  remove_key_from_buffer({arg.key, EventTrigger::OnKeyPressed});
  events_.push_back({arg.key, EventTrigger::OnKeyReleased});
   return true;
}

bool
EventListener::mouseMoved(const OIS::MouseEvent& evt) {
  CEGUI::GUIContext& context = CEGUI::System::getSingleton().getDefaultGUIContext();
  context.injectMouseMove(evt.state.X.rel, evt.state.Y.rel);
  return true;
}

bool
EventListener::mousePressed(const OIS::MouseEvent& evt,
                            OIS::MouseButtonID id) {
  CEGUI::GUIContext& context = CEGUI::System::getSingleton().getDefaultGUIContext();
  context.injectMouseButtonDown(convertMouseButton(id));
  x = evt.state.X.abs;
  y = evt.state.Y.abs;
  mouse_key_ = {id, EventTrigger::OnKeyPressed};
  return true;
}

bool
EventListener::mouseReleased(const OIS::MouseEvent& evt,
                             OIS::MouseButtonID id) {
  CEGUI::GUIContext& context = CEGUI::System::getSingleton().getDefaultGUIContext();
  context.injectMouseButtonUp(convertMouseButton(id));
  return true;
}

void
EventListener::windowClosed(Ogre::RenderWindow* window){
  exit_ = true;
}

void
EventListener::create_input_manager(Ogre::RenderWindow* window) {
  typedef std::pair<std::string, std::string> parameter;
    OIS::ParamList parameters;
    size_t xid = 0;

    window->getCustomAttribute("WINDOW", &xid);
    parameters.insert(parameter("WINDOW", std::to_string(xid)));
    parameters.insert(parameter("x11_mouse_grab", "false"));
    parameters.insert(parameter("x11_mouse_hide", "false"));
    parameters.insert(parameter("x11_keyboard_grab", "false"));
    parameters.insert(parameter("XAutoRepeatOn", "false"));

    input_manager_ = OIS::InputManager::createInputSystem(parameters);
}

void
EventListener::remove_key_from_buffer(KeyBoardKey event) {
    auto keyevent = find (events_.begin(), events_.end(), event);
    if(keyevent == events_.end())
      return;
    events_.erase(keyevent);
}

CEGUI::MouseButton
EventListener::convertMouseButton(OIS::MouseButtonID id) {
  CEGUI::MouseButton cegui_id;
  switch(id)
    {
    case OIS::MB_Left:
      cegui_id = CEGUI::LeftButton;
      break;
    case OIS::MB_Right:
      cegui_id = CEGUI::RightButton;
      break;
    case OIS::MB_Middle:
      cegui_id = CEGUI::MiddleButton;
      break;
    default:
      cegui_id = CEGUI::LeftButton;
    }
  return cegui_id;
}
