// -*- coding:utf-8; tab-width:4; mode:cpp -*-
// Copyright (C) 2014  ISAAC LACOBA MOLINA
// Tinman author: Isaac Lacoba Molina
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef RACE_H
#define RACE_H
#include <memory>
#include <utility>

#include "player.h"
#include "timer.h"
#include "powerup.h"

class Game;

class Race {
  std::shared_ptr<Game> game_;

  int number_of_laps_;
  PowerUp::shared nitro_, money_;
  std::vector<Player::shared> players_;
  Track::shared circuit_;
  std::vector<std::string> player_nicks_;

 public:
  typedef std::shared_ptr<Race> shared;

  std::vector<btVector3> starting_grid_;

  Timer timer_;

  Race(std::string data_file);
  virtual ~Race();

  void update(float delta);

  void realize(std::shared_ptr<Game> game, std::vector<Player::shared> players);

  void add_collision_hooks();

  void config_race();
  void start();
  void pause();
  void reset();

  void next(std::string data_file);

  void add(std::vector<Player::shared> players);
  bool has_ended();

  Section get_section(int index);

 private:
  void create_powerups(void);

  void configure_grid();
  void update_players(float delta);
  bool end(std::vector<Car::shared> cars);
  Player::shared get_player(std::string nick);
  void config_cars_in_login_order();
  void reset_powerups();
  void reset_cars();
  std::vector<std::pair<std::string, int>> get_players_positions();

  void add_collision_with_circuit(Car::shared car);
  void add_collision_with_cars(Player::shared target);
};
#endif
