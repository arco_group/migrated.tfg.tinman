// -*- coding:utf-8; tab-width:4; mode:cpp -*-g
// Copyright (C) 2014  ISAAC LACOBA MOLINA
// Tinman author: Isaac Lacoba Molina
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "game.h"

Session::Session(Race::shared new_race) {
  race_ = new_race;
  track_ = 0;
}

Session::Session(Race::shared new_race, Player::shared player) {
  race_ = new_race;
  human_player_ = player;
  players_.push_back(human_player_);
  track_ = 0;
}

void
Session::add_bot(std::string nick) {
  players_.push_back(BotFactory::create(nick, game_->physics_,
					game_->sound_, game_->animation_,
					race_, game_->car_factory_->create(nick)));
}

void
Session::add_network_player(std::string nick) {
  Car::shared car = game_->car_factory_->create(nick);
  players_.push_back(NetworkFactory::create(nick, game_->sound_,
					    game_->animation_, car));
}

void
Session::add_instrumentated_player(std::string nick) {
  std::cout << "[Session] " << __func__ << std::endl;
  players_.push_back(InstrumentatedFactory::create(nick, game_->sound_,
			   game_->animation_, game_->car_factory_->create(nick)));
}

void
Session::add_remote_players(Tinman::LoginInfo login_info) {
  for(auto nick: login_info.playerNicks) {
    if(human_player_->nick_ == nick)
      players_.push_back(human_player_);
    else
      add_network_player(nick);
  }
}

void
Session::realize() {
  std::cout << "[Session] realize" << std::endl;
  create_overlays();
  configure_race();
}

void
Session::create_overlays() {
  timer_ = game_->gui_->create_overlay("Info", "Timer");
  lap_overlay_ = game_->gui_->create_overlay("Info", "Lap");
  nitro_overlay_ = game_->gui_->create_overlay("Info", "Nitro");
  position_overlay_ = game_->gui_->create_overlay("Info", "Position");
}

void
Session::update(float delta) {
  game_->animation_->update(delta);

  game_->physics_->step_simulation(delta, 64);
  game_->physics_->check_collision();

  race_->update(delta);
  update_overlays();
}

void
Session::update_overlays() {
  float time = race_->timer_.get_time_since_start();
  timer_->setCaption(to_string_with_precision(time, 3));
  if(!human_player_)
    return;
  nitro_overlay_->setCaption(std::to_string(human_player_->get_car()->get_nitro()));
  lap_overlay_->setCaption(std::to_string(human_player_->get_car()->get_lap()));
  update_ranking();
}

void
Session::update_ranking() {
  int human_player_position = get_human_player_position();
  std::stringstream position;
  position << human_player_position + 1 << "/" << players_.size();
  position_overlay_->setCaption(position.str());
}

Ice::AsyncResultPtr
Session::login() {
  game_->mode_ =  Game::Mode::Network;

  human_player_->set_socket({game_->ip_, game_->port_});
  return human_player_->login(shared_from_this());
}

Tinman::LoginInfo
Session::complete_login(Ice::AsyncResultPtr login) {
  login_info_ = human_player_->complete_login(login);
  add_remote_players(login_info_);
  std::cout << "recibido informacion de los otros jugadores del servidor" << std::endl;

  return login_info_;
}

void
Session::disconnect() {
  human_player_->disconnect();
}

std::string
Session::to_string_with_precision(const float number, const int n) {
  std::ostringstream number_stringify;
  number_stringify << std::setprecision(n) << number;
  return number_stringify.str();
}

void
Session::store(Tinman::Snapshot snapshot) {
  for(auto car_info: snapshot)
    get_player(car_info.carId)->store(std::move(car_info));
}

void
Session::reset() {
  game_->physics_->clear_triggers();

  reset_race();
}

void
Session::reset_race() {
  race_->reset();
  std::stringstream circuit;
  circuit << "config/circuit" << ++track_ << ".data";
  race_->next(circuit.str());
}

void
Session::configure_race() {
  race_->realize(game_, players_);
}

int
Session::get_human_player_position() {
  std::sort(game_->race_results_.begin(), game_->race_results_.end(),
            [](auto player1, auto player2) {
              return player1.second > player2.second;
            });

  auto human_player = std::find_if(game_->race_results_.begin(),
	   game_->race_results_.end(),
           [&](auto player) {
             return player.first == human_player_->get_nick();
           });
  return std::distance(game_->race_results_.begin(), human_player);
}

Player::shared
Session::get_player(Ice::Byte id) {
  if(human_player_->get_id() == id)
    return human_player_;

  for(Player::shared player: players_)
    if(player->get_id() == id)
      return player;

  return nullptr;
}
