// Copyright (C) 2014  ISAAC LACOBA MOLINA
// Tinman author: Isaac Lacoba Molina
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#ifndef CAR_H
#define CAR_H
#include <tuple>
#include <queue>

#include "physics.h"
#include "scene.h"
#include "carcontroller.h"
#include "sound.h"
#include "objecttextdisplay.h"
#include "track.h"

#include "tinman.h"

bool equals(float sut, float target, float delta);
bool equals(btVector3 sut, btVector3 target, float delta);
class lower_than {
  bool reverse_;
 public:
  lower_than( const bool& reverse=false){ reverse_ = reverse;}
  bool operator() (const Tinman::CarInfo& first, const Tinman::CarInfo& second) const {
    return (reverse_)? (first.seqNumber > second.seqNumber):
      (first.seqNumber < second.seqNumber);
  }
};

class Car {
 public:
  typedef std::shared_ptr<Car> shared;
  CarController::shared controller_;
  int race_position_;
  Ice::Byte id_;
  int lap_;
  Ogre::SceneNode* chassis_node_;
  btRigidBody* chassis_body_;
  Section current_segment_;

  bool colliding_, stuck_;

  Car(std::string nick);
  virtual ~Car();

  void realize(Scene::shared scene, std::string name, std::string material,
               Ogre::Vector3 position, int scale);
  void realize(Physics::shared physics, Scene::shared scene,
               Sound::shared sound, std::string name, btVector3 position,
               std::string material, int checkpoints, Ice::Byte id);
  void realize(Physics::shared physics, Sound::shared sound, std::string name,
               btVector3 position, Ice::Byte id);

  void exec(Tinman::Action action);
  void accelerate();
  void use_nitro();
  void stop_accelerating();
  void brake();
  void stop_braking();
  void turn(Direction direction);
  void turn_right();
  void turn_left();
  void stop_turning();

  void update(float delta);
  void synchronize(Tinman::CarInfo last_update);

  int get_nitro();
  int get_lap();
  btVector3 get_position();
  btVector3 get_velocity();
  btQuaternion get_orientation();
  btVector3 get_direction(int factor = 1);
  btScalar get_speed();
  Section get_current_section();

  void set_position(btVector3 position);
  void set_race_segment_info(Section segment);
  void reset(int checkpoints, btVector3 position);
  void collide_with_car();
  void animation(float delta);

  bool is_colliding();
  bool is_stuck();
  void invert_direction();

 private:
  Sound::shared sound_;
  Physics::shared physics_;
  std::map<Tinman::Action, std::function<void()>> action_hooks;
  const btQuaternion initial_rotation_ = btQuaternion(btVector3(0, 1, 0), btScalar(80));
  btVector3 initial_position_;
  unsigned int sections_covered_;
  bool accelerating_;
  Tinman::CarInfo last_update_;

  std::string nick_;
  ObjectTextDisplay* nickname_display_;

  float invert_direction_, not_moving_, colliding_time_;

  bool can_collide_;
  const float invert_direction_delay_ = 0.48f;
  const float not_moving_delay_ = 1.f;
  const float colliding_delay_ = 1.f;

  std::priority_queue<Tinman::CarInfo, std::vector<Tinman::CarInfo>, lower_than> sync_buffer_;
  std::vector<std::pair<int, bool>> checkpoints_;

  btCompoundShape* compound_;
  btAlignedObjectArray<btCollisionShape*> collision_shapes_;

  btRaycastVehicle::btVehicleTuning       tuning_;
  btVehicleRaycaster*                     vehicle_raycaster_;
  btRaycastVehicle*                       vehicle_;
  btCollisionShape*                       wheel_shape_;

  Ogre::Entity* chassis_entity_;
  std::vector<Ogre::SceneNode*> wheels_nodes_;
  std::vector<Ogre::Entity*> wheels_entities_;

  btVector3 last_position_, last_velocity_;
  btQuaternion last_orientation_;

  void init_graphic_bodies(Scene::shared scene,  std::string name, std::string,
                           Ogre::Vector3 position = Ogre::Vector3(0,0,0), int scale = 1);
  void init_physic_bodies(Physics::shared physics, btVector3 position);
  void init_raycast_car(Physics::shared physics);
  void add_graphic_wheel(Scene::shared scene,  std::string parent, std::string name,
                         int scale = 1);
  void add_physic_wheel(bool is_front, btVector3 connection_point,
                        int wheel_index);
  void add_nick_billboard(Scene::shared scene);
  void add_wheels();
  void configure_wheels();

  void turn_wheels(Direction direction);
  void control_speed();

  void reset_position();

  void apply_update(bool delete_update);
  void update_lap();


  bool not_moving();
  bool is_on_meta();
  bool has_done_a_lap();
  void add_lap();
};

#endif
