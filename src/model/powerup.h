// Copyright (C) 2014  ISAAC LACOBA MOLINA
// Tinman author: Isaac Lacoba Molina
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef POWER_UP_H
#define POWER_UP_H

#include <chrono>
#include <thread>
#include <future>

#include "scene.h"
#include "physics.h"
#include "animation.h"
#include "meshstrider.h"
#include "parser.h"

class PowerUp {
  std::string file_;
  Animation::shared animation_;
  Scene::shared scene_;
  Physics::shared physics_;

  Ogre::SceneNode* node_;
  Ogre::Entity* entity_;
  btCollisionShape* shape_;

 public:
  typedef float seconds;
  typedef btVector3 Position;
  typedef std::pair<Position, seconds> Location;
  enum class Type {Nitro, MoneyBag};
  typedef std::shared_ptr<PowerUp> shared;

  btRigidBody* body_;

  PowerUp(Scene::shared scene, Physics::shared physics,
          Animation::shared animation);
  virtual ~PowerUp();

  void realize(Type object);
  void update(float delta);
  void pick_up();
  void reset();

  void active_animation(std::string animation_name);

 private:
  std::vector<Location> locations_;
  Location last_location_;

  void read_locations(std::string file);

  float start_;
  bool removed_;

  void replace(btVector3 new_position);
  void add_to_scene();
  void remove_from_scene();
};
#endif
