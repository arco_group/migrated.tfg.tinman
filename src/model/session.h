// Copyright (C) 2014  ISAAC LACOBA MOLINA
// Tinman author: Isaac Lacoba Molina
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef SESSION_H
#define SESSION_H
#include <OgreTextAreaOverlayElement.h>

#include <algorithm>

#include "physics.h"
#include "car.h"
#include "race.h"
#include "player.h"

class Game;

class Session: public std::enable_shared_from_this<Session> {
  Tinman::LoginInfo login_info_;

  Ogre::OverlayElement *timer_, *lap_overlay_, *nitro_overlay_, *position_overlay_;
  PowerUp::shared nitro_, money_;
  /* std::pair<std::string, int> socket_; */
  std::vector<Tinman::Snapshot> snapshots_;

  int track_;
  std::vector<std::pair<std::string, int>> players_positions_;

 public:
  std::shared_ptr<Game> game_;
  Race::shared race_;
  std::vector<std::string> players_nicks_;
  Player::shared human_player_;
  std::vector<Player::shared> players_;

  typedef std::shared_ptr<Session> shared;

  Session(Race::shared race);
  Session(Race::shared new_race, Player::shared player);

  void add_bot(std::string nick);
  void add_network_player(std::string nick);
  void add_instrumentated_player(std::string nick);

  void update(float delta);
  void realize();
  bool race_ended();

  void next_race();

  void store(Tinman::Snapshot snapshot);

  Ice::AsyncResultPtr login();
  Tinman::LoginInfo complete_login(Ice::AsyncResultPtr login);

  void reset();
  void disconnect(void);

 private:
  Player::shared get_player(std::string nick);
  Player::shared get_player(Ice::Byte id);

  void add_remote_players(Tinman::LoginInfo login_info);

  void create_overlays(void);
  void config_cars_in_login_order(void);

  void update_players(float delta);
  void update_overlays(void);
  void update_ranking();

  std::string to_string_with_precision(const float number, const int n);

  void configure_race();

  void reset_race();
  void active_animation(std::string node, std::string animation_name);

  int get_human_player_position();
};
#endif
