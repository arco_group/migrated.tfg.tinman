// -*- coding:utf-8; tab-width:4; mode:cpp -*-
// -*- coding:utf-8; tab-width:4; mode:cpp -*-
// Copyright (C) 2014  ISAAC LACOBA MOLINA
// Tinman author: Isaac Lacoba Molina
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "track.h"

Track::Track(std::string file) {
  file_name_ = file;
  x_ = columns_ = 0;
  z_ = -60;

  builder_[u'\u2500'] = std::bind(&Track::create_section, this, "rect1");
  builder_[u'\u2502'] = std::bind(&Track::create_section, this, "rect2");
	builder_[u'\u256D'] = std::bind(&Track::create_section, this, "curve-right2");
	builder_[u'\u2570'] = std::bind(&Track::create_section, this, "curve-right1");
  builder_[u'\u256E'] = std::bind(&Track::create_section, this, "curve-left2");
  builder_[u'\u256F'] = std::bind(&Track::create_section, this, "curve-left1");
  builder_[u'\u257E'] = std::bind(&Track::create_section, this, "ramp1");
  builder_[u'\u257F'] = std::bind(&Track::create_section, this, "ramp2");
  builder_[u'\u2505'] = std::bind(&Track::create_section, this, "meta");
  init_ = false;
}

void
Track::open_file(std::string file) {
  file_.open(file);
  file_.imbue(std::locale(""));
  std::cout << "open file: " << file << std::endl;
  if (!file_.is_open()) {
    std::cerr << "no file: \n"
              << file
              << std::endl;
    exit(-1);
  }
}

void
Track::next(std::string file) {
  index_ = 0;
  open_file(file);

  std::wstring line;
  while(getline(file_, line)) {
    for(auto circuit_segment: line){
      if(builder_[circuit_segment]){
        current_char_ = circuit_segment;
        segments_.push_back(builder_[circuit_segment]());
      }
      x_ += 20;
      index_++;
    }
    z_ += 20;
    columns_ = x_ / 20;
    x_ = 0;
  }
  z_ = -60;
  x_ = 0;
  file_.close();
  order_track();
  add_stairs();
}

void
Track::next(Scene::shared scene, Physics::shared physics) {
  std::cout << "[Track] next" << std::endl;
  scene_ = scene;
  physics_ = physics;
  next(file_name_);
}


Section
Track::create_section(std::string name) {
  auto graphic_element = create_graphic_element(name);
  return create_physic_element(name, graphic_element);
}

Ogre::SceneNode*
Track::create_graphic_element(std::string name) {
  std::stringstream mesh_name, node_name;
  node_name << name << x_ << z_;
  mesh_name << name << "-render.mesh";
  Ogre::Vector3 position(x_, 0, z_);

  return scene_->create_graphic_element(position, node_name.str(), mesh_name.str());
}

Section
Track::create_physic_element(std::string name, Ogre::SceneNode* graphic_element) {
  std::stringstream mesh_name, entity_name;
  mesh_name << name << "-physics.mesh";
  entity_name << name << x_ << z_ << "physics";
  Ogre::Entity* physic_entity = scene_->create_entity(entity_name.str(),
                                                      mesh_name.str(), false);
  btCollisionShape* ground_shape =
    physics_->create_shape(new MeshStrider(physic_entity->getMesh().get()));
  btVector3 origin = btVector3(x_, 1, z_);
  btQuaternion rotation = btQuaternion(btVector3(0, 1, 0),
                                       btScalar(0));
  btRigidBody* plane_body =
    physics_->create_rigid_body(btTransform(rotation, origin),
                              ground_shape, 0);

  plane_body->setRestitution(0.2);
  plane_body->setFriction(0.5f);
  return Section{get_section_type(name), graphic_element, physic_entity, plane_body,
      origin, index_, directions_[current_char_]};
}

Section::Type
Track::get_section_type(std::string name) {
  if(name == "curve-right1")
    return Section::Type::RightCurve1;

  if(name == "curve-right2")
    return Section::Type::RightCurve2;

  if(name == "curve-left1")
    return Section::Type::LeftCurve1;

  if(name == "curve-left2")
    return Section::Type::LeftCurve2;

  if(name == "rect1" || name == "ramp1")
    return Section::Type::Rect1;

  if(name == "rect2" || name == "ramp2")
    return Section::Type::Rect2;

  return Section::Type::Meta;
}

void
Track::reset() {
  std::cout << "[Track] " << __func__ << std::endl;
  for(auto section: segments_) {
    scene_->destroy_node(section.node);
    scene_->destroy_entity(section.physics_entity);
    physics_->remove_rigid_body(section.body);

    delete section.body->getCollisionShape();
    delete section.body;
  }
  segments_.clear();

  for(auto node: stairs_)
    scene_->destroy_node(node);
}

void
Track::order_track() {
  auto meta = std::find_if(segments_.begin(), segments_.end(), [&](auto segment){
      return segment.type == Section::Type::Meta;
    });
  int id = meta->id;
  meta->id = 0;
  char direction = '\0';
  int last_id = id;
  char last_direction = 'L';
  for(int index = 1; index < segments_.size(); index++) {
    direction = get_direction(segments_[id].type, segments_[last_id].type, last_direction);
    last_direction = direction;
    last_id = id;
    id = get_next_section(id, direction);
    // std::cout << "index: " << index
    //           << "id: " << id
    //           << " last_id: " << last_id  << std::endl;

    segments_[id].id = index;
  }
  std::sort(segments_.begin(), segments_.end(), [&](auto segment1, auto segment2){
      return segment1.id < segment2.id;
    });
}

int
Track::get_next_section(int index, char direction) {
  int x = index % columns_;
  int y = index / columns_;
  // std::cout << " x: " << x << "y: " << y
  //           << " columns: " << columns_
  //           << " direction: " << direction;
  if(direction == 'L')
    x -= 1;
  if(direction == 'R')
    x += 1;
  if(direction == 'U')
    y -= 1;
  if(direction == 'D')
    y += 1;

  return (x + y*columns_) % segments_.size();
}

char
Track::get_direction(Section::Type type, Section::Type previous, char last_direction){
  if(type == Section::Type::Rect1){
    if(previous == Section::Type::RightCurve1 ||
       previous == Section::Type::RightCurve2 ||
       last_direction == 'R')
      return 'R';
    return 'L';
  }

  if (type == Section::Type::Rect2) {
    if (previous == Section::Type::LeftCurve2 ||
        previous == Section::Type::RightCurve2 ||
        last_direction == 'D')
      return 'D';
    return 'U';
  }

  if (type == Section::Type::RightCurve1) {
    if (previous == Section::Type::Rect2 ||
        previous == Section::Type::RightCurve2 ||
        last_direction == 'D')
      return 'R';
    return 'U';
  }

  if (type == Section::Type::RightCurve2){
    if (previous == Section::Type::Rect1 ||
        previous == Section::Type::LeftCurve1)
      return 'D';
    return 'R';
  }

  if (type == Section::Type::LeftCurve1) {
    if (previous == Section::Type::Rect2 ||
        previous == Section::Type::LeftCurve2 ||
        last_direction == 'D')
      return 'L';
    return  'U';
  }

  if (type == Section::Type::LeftCurve2) {
    if (previous == Section::Type::Rect2 ||
        previous == Section::Type::LeftCurve1 ||
        last_direction == 'U')
      return 'L';
    return 'D';
  }
  return 'L';
}

void
Track::add_stairs() {
  std::cout << "[Track] " << __func__ << std::endl;
  stairs_.push_back(scene_->create_plane("Y", "ground", "Ground", "",
					           "BackgroundGround"));
  std::stringstream name;

  stairs_.push_back(scene_->create_graphic_element(Ogre::Vector3(-2, 4, -78),
						    "stairx0", "cornerstair.mesh"));


  for (int x = 14; x < 80; x += 16) {
    name.str("");
    name << "stairx" << x;
    stairs_.push_back(scene_->create_graphic_element(Ogre::Vector3(x, 4, -78),
						      name.str(), "stair.mesh"));
  }


  for (int z = 3; z > -60; z -= 16) {
    name.str("");
    name << "stairz" << z;
    auto node = scene_->create_graphic_element(Ogre::Vector3(-17.44, 4.72, z),
					       name.str(), "stair.mesh");
    node ->yaw(Ogre::Degree(90));
    stairs_.push_back(node);
  }

  auto node = scene_->create_graphic_element(Ogre::Vector3(98, 4, -62),
					     "stairx80", "cornerstair.mesh");
  node->yaw(Ogre::Degree(-90));
  stairs_.push_back(node);

  for (int z = 1; z > -60; z -= 16) {
    name.str("");
    name << "stair-z" << z;
    auto node = scene_->create_graphic_element(Ogre::Vector3(98, 4, z),
					       name.str(), "stair.mesh");
    node->yaw(Ogre::Degree(-90));
    stairs_.push_back(node);
  }
}
