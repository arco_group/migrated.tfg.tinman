En esta segunda iteración se pretende crear el primer test. Se comenzó a partir
del conocimiento adquirido sobre Ogre3D en la iteración anterior con la
intención de crear un ejemplo mínimo auto-contenido, que sirva por un lado para
mostrar el proceso de inicialización del motor de renderizado, y por otro, para
comprobar que se inicializa correctamente cada vez que se ejecute el juego.

\subsection{Análisis}
\label{sec:analisis-1}
El objetivo es crear una prueba que permita comprobar que el proceso de
inicialización del motor de renderizado se lleva a cabo correctamente. En este
caso, se considera que lo mas sencillo pasa por renderizar un coche sobre un
escenario vacío. A grandes rasgos, el test asociado creará los objetos
necesarios para poder inicializar Ogre, seguirá añadiendo el coche a la escena
y, por último, se llamará a la sentencia que renderizará la escena, lo que
mostrará un coche sobre un escenario vacío.

En este punto existe un problema, al que se debe dar solución:

\begin{itemize}
\item ¿De qué forma se puede comprobar que la prueba efectivamente se ha
  ejecutado según lo esperado? Es decir, ¿se está renderizando un coche sobre un
  fondo negro, o lo que se ve en pantalla es otra cosa?
\end{itemize}

Un videojuego, en gran parte, está formado por una serie de algoritmos, de
inteligencia artificial, comunicaciones en red, simulación física, etcétera, que
no se diferencian mucho de otros algoritmos que se pudiesen encontrar en otro
tipo de software. Por esta razón, realizar pruebas unitarias sobre las clases
del motor de juego no tiene mayor dificultad. Sin embargo, ya que un videojuego
no es sino una aplicación de renderizado interactivo, realizar pruebas de
integración (o de sistema) sobre uno tiene varios problemas:

\begin{itemize}
\item Problemas de precisión en cálculos en coma flotante: De forma habitual
  cuando se trabaja con bibliotecas gráficas, de red o motores de simulación
  física se realizan cálculos en coma flotante. El problema radica en que existe
  un pequeño error en estos cálculos debido a problemas de redondeo del punto
  flotante, variando los resultados obtenidos dependiendo de la arquitectura del
  procesador, del set de instrucciones, etcétera. Esto significa que con la
  misma entrada no se puede garantizar que se vaya a generar la misma salida de
  forma exacta. Debido a esto, para comparar valores en coma flotante se
  debe trabajar con intervalos o truncar el valor de salida, ya que una
  comparación entre dos valores que teóricamente deberían ser iguales hará que
  la comparación falle.
\item Dependencia entre módulos del motor de juego: debido a que típicamente en
  un motor de juego la mayoría de los módulos que lo conforman dependen de otros
  módulos, que a su vez dependen de otros módulos, realizar una prueba sobre un
  módulo concreto obliga a arrancar todo el motor de juego; por ejemplo, para
  probar que el comportamiento de un cuerpo dentro de la escena es físicamente
  aceptable se deberá arrancar el motor de físicas, pero este depende del motor
  de renderizado, ya que en un videojuego no tiene sentido no renderizar ninguna
  imagen. El problema de esto es que es mucho mas complejo depurar errores
  debido a que no se puede estar seguro de qué módulo es el que está
  produciéndolos. Idealmente se debería lanzar únicamente el módulo que se
  quiere probar, de forma que intervenga el mínimo código fuente posible durante
  la ejecución del test en cuestión.
\item Necesidad de feedback del jugador: normalmente el bucle principal del
  juego está orientado a los eventos que generará un jugador mediante un
  teclado/ratón o un joystick. Esto significa que es el jugador el que provoca
  los cambios de estado en el videojuego. Si se pretende que la prueba simule el
  comportamiento de un jugador, haciendo lo que uno haría, se debe añadir al
  motor una serie de mecanismos de instrumentación que permitan simular esos
  eventos de entrada. Esto hace que no sea posible realizar pruebas que simulen
  el comportamiento de un jugador de forma inmediata, lo que complica la
  creación y ejecución de pruebas.
\item Dificultad y lentitud a la hora de comprobar que las imágenes renderizadas
  son las esperadas: una de las características mas importantes que debe tener
  un test es que su tiempo de ejecución debe que ser mínimo, ya que deberá ser
  ejecutado cada vez que se quiera lanzar la aplicación para detectar posibles
  errores introducidos en el nuevo código que se va escribiendo. Unos de los
  problema de los algoritmos de visión por computador es que son costosos y
  lentos. Dado que la idea a la hora de crear pruebas es ejecutarlas tras cada
  modificación en el código, habría que esperar una cantidad no despreciable de
  tiempo, lo que ralentizaría el proceso de desarrollo. Otro de los problemas de
  esta aproximación es que se estaría dependiendo completamente de un escenario
  concreto, corriendo el riesgo de que la prueba produzca el resultado previsto
  pero el código de producción no.
\end{itemize}

\begin{figure}[h]
  \centering
  \includegraphics[width=0.8\textwidth]{modelado-coche.png}
  \caption{Resultado final del modelado del coche}
  \label{fig:modelado-coche}
\end{figure}

\subsection{Diseño e Implementación}
\label{sec:implementacion-1}

La solución que aquí se plantea pasa por utilizar un log para registrar eventos
importantes sucedidos durante el proceso de inicialización de nuestro motor de
juego y comprobar en la prueba que dichos mensajes se han registrado. Esta
aproximación no es todo lo flexible que se desearía, ya que lo que se está
haciendo en realidad consiste en comprobar que el log tiene registradas unas
cadenas de texto concretas. Sin embargo, esto soluciona el problema de comprobar
que el proceso de inicialización del motor de juego se realiza correctamente.

Como biblioteca de Logs, se ha utilizado Boost.Log~\cite{boost-log-manual}. Se
ha optado por el proyecto Boost por que es un estándar de facto dentro del
lenguaje de programación C++. Muchas de las características de Boost se han ido
introduciendo en los últimos versiones oficiales de C++, como los \emph{smart
  pointers} que se encontraban implementados en la biblioteca del proyecto Boost
muchos años antes de que se incorporaran a C++.

El resultado de esta iteración se puede encontrar en el directorio
\emph{example/00\_render\_a\_car}\footnote{ver \url{https://goo.gl/mdbFBs}}. En
la figura~\ref{fig:iteration1} se muestra el resultado de la prueba y
en~\ref{fig:modelado-coche} el resultado final del proceso de modelado del coche
que se ha creado apropósito para este proyecto.

En el listado\ref{test-1} se muestra el código de esta primera prueba, la cuál
comprueba el estado del motor de juego tras el proceso de inicialización, la
cuál es parecida a los mencionado en la sección~\ref{sec:test-autom-en} del
capítulo de antecedentes:
\begin{listing}[language = C++,
  caption = {Primer test},
  label = test-1]
      describe("a game", []() {
        std::ifstream game_log;
        std::vector<std::string> messages = {"Ogre::Root initialized", "Resources loaded",
          "SceneManager initialized", "Ogre::Camera initialized",
          "Ogre::Light initialized", "Car added", "Car graphic body initialized",
          "Car physic body initialized", "Car initialized", "RenderLoop
          started", "RenderLoop ended"};

        before_each([&]() {
            Log log;
            Game::shared game;

            game->new_session();
            game->session_->add(std::make_shared<Car>());
            game->session_->realize();

            int seconds = 1;
            game->loop(seconds);

            log.flush();
            game_log.open("config/game.log");
          });

        it("should log information",[&]() {
            std::string message;
            for (int i = 0; std::getline(game_log, message); ++i) {
              Assert::That(message, Equals(messages[i]));
            }

            game_log.close();
          });
  \end{listing}

La prueba realiza los siguientes pasos:
\begin{itemize}
\item En el setup de la prueba(\texttt{before\_each()}) se crea un \emph{smart
    pointer} de la clase Game, que es la clase que engloba el bucle de
  renderizado. Esta clase representa una instancia del juego.
\item Tras esto se añade una nueva sesión al juego. La clase \emph{Session}
  representa una sesión de juego, cuya duración va desde que comienza la partida
  hasta que finaliza.
\item A esta sesión se le añade un coche y se llama al método
  \texttt{realize()}, que inicia el proceso de configuración de la clase
  \emph{Session}. La función de dicho método es la de configurar todos los
  atributos de la clase Session.
\item El siguiente paso consiste en invocar al bucle de juego para que de
  comienzo el juego, con una duración de un segundo. Esto dará como resultado la
  imagen renderizada del coche que se añadió a la sesión. Lo importante es que
  el juego esté activo el tiempo suficiente como para que se ejecuten todas las
  rutinas que engloban al bucle al menos una vez.
\item Tras esto, se utiliza la directiva flush del Log, de forma que sincronice
  el contenido del fichero de texto con el contenido de los \emph{Sinks} del
  Log.
\item Después se abrirá dicho fichero de texto, tras lo cuál se comparará que cada una de las
  líneas contengan la frase esperada.
\end{itemize}

La prueba se ha escrito utilizando el framework de testing \emph{Bandit}. Cada
una de las directivas que se ven en la prueba( \emph{describe},
\emph{before\_each}, \emph{it}) son funciones que reciben como parámetro una
función lambda. Este framework hace uso de características propias del nuevo
estándar de C++11.

\begin{figure}[h]
  \centering
  \includegraphics[width=0.8\textwidth]{iteration1.png}
  \caption[Resultado de la iteración 1]{Resultado de la iteración 1}
  \label{fig:iteration1}
\end{figure}
