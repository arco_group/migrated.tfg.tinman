En esta iteración se procederá a realizar una implementación del modelo de red
del juego. Se estudiarán los diferentes modelos de red existentes en
videojuegos, a fin de escoger el que mejor se adapte a las características de
este proyecto. Por otra parte, se priorizará alcanzar una correcta jugabilidad
en \acs{LAN}, implementando los mecanismos en red necesarios.

\subsection{Análisis}
\label{sec:analisis-8}
En la sección~\ref{sec:intr-los-jueg} se introducían los conceptos de
\emph{consistencia} e \emph{inmediatez}. En dicha sección se definía
la \emph{inmediatez} como el tiempo que le lleva al juego representar
las acciones del jugador.

El videojuego que se trata de desarrollar necesita minimizar el tiempo
de respuesta, ya que un juego de carreras se caracteriza por tener
movimientos rápidos, de forma que los jugadores esperan poder
controlar el coche de una forma fluida. Por tanto, se hace necesario
escoger un modelo de red que permita maximizar la \emph{inmediatez}.

No existe mucha literatura que clasifique los diferentes modelos de
red. En la sección~\ref{sec:modelos-de-red} se mostraba una
clasificación creada por \emph{Glenn Fiedler}. En esta sección se
intentará mostrar bajo qué circunstancias es mejor usar un modelo u
otro:
\begin{itemize}
\item \emph{Determistic Lockstep}~\cite{deterministic-lockstep}: es
  apropiado en situaciones donde el motor de físicas es totalmente
  determinista, o la simulación física no es un problema. Se
  caracteriza por sincronizar el juego usando únicamente las acciones
  de cada uno de los jugador de la partida, delegando al motor de
  físicas la labor de sincronización. Cada jugador envía las acciones
  que ha realizado y espera recibir las acciones de los demás
  jugadores. La ventaja de esta aproximación es que el ancho de banda
  consumido es mínimo e independiente del número de objetos físicos
  que haya en la escena, ya que toda la información que se envía y se
  recibe son las acciones de cada uno de los demás jugadores.

\item Basados en interpolación~\cite{interpolation}: modelo que
  históricamente se ha usado en situaciones donde, por motivos de
  recursos, no se quería utilizar motor de físicas en el cliente. Se
  caracteriza por enviar la información mas importante cada frame
  (snapshots) e ir almacenando en un bufer dichos resúmenes del
  estado global del juego para poder interpolar el estado local de la
  partida del cliente. Es apropiado cuando el número de jugadores
  aumenta por encima de 4, ya que permite una sincronización mucho mas
  precisa que la que se consigue con el método anterior.

\item \emph{State Synchronization}: al contrario que el modelo
  anterior, en este se envía el estado de los objetos físicos mas
  importantes de la escena a una tasa lo mas alta posible. En este
  modelo se hace uso del motor de físicas pero no se confía plenamente
  en él (como se hacía en el primer método), sino que se envía algunos
  datos clave que permiten sincronizar las diversas instancias de
  juego. Se usa un modelo de prioridades para determinar cuál de los
  objetos del juego tiene una mayor necesidad de sincronización.
\end{itemize}

Cada una de las técnicas de sincronización anteriores tienen ventajas
e inconvenientes que los hacen mas apropiados dependiendo de las
circunstancas de cada proyecto:
\begin{itemize}
\item El modelo basado en interpolación tiene el inconveniente de que
  no es trivial interpolar el movimiento de un vehículo de una forma
  fiel al movimiento real. Debido a las propiedades físicas del
  movimiento de un vehículo, el movimiento normal del mismo en un
  terreno lleno de baches, como es el caso de los circuitos que se han
  modelado, consiste en oscilaciones del chasis en vertical y
  permanentes saltos, así como cambios bruscos de dirección. En otros
  tipos de juegos tan exigentes como pueden ser los FPS, en donde los
  cambios de dirección también son habituales, la simulación física es
  mucho mas sencilla debido a que la forma en que tenemos de andar las
  personas normalmente no implica variaciones de altura; es decir, el
  movimiento del personaje puede ser simulado perfectamente en un
  plano 2D y añadir una animación puramente estética. Por otra parte,
  debido a la gran cantidad de información necesaria para poder
  interpolar correctamente el estado de la escena (el modelo está
  pensado para ser usado en situaciones donde no se ejecuta el motor
  de físicas en el cliente) se tiene que comprimir los datos para
  reducir el ancho de banda.
\item Debido a que la simulación física es tan compleja, no tiene
  sentido utilizar el modelo \emph{deterministic lockstep}, ya que en
  poco tiempo se desincronizaría.
\item Por tanto, de lo anterior se deduce que el modelo que mejor se adapta a
  las necesidades es el método \emph{state synchronization}.
\end{itemize}

Además, hay que tener en cuenta todos los problemas de asincronismo
derivados de las comunicaciones en red. El videojuego tiene que ser
capaz de gestionar la recepción de paquetes de forma asíncrona, de
forma que no se bloquee. Para ello habrá que hacer uso de técnicas de
programación concurrente, como son hilos y monitores, que servirán
para realizar un acceso concurrente a los buffers donde se almacenaran
los mensajes antes de ser gestionados, que evite que se bloquee el
hilo principal de procesamiento.

\subsection{Diseño e Implementación}
\label{sec:implementacion-8}
A la hora de implementar el modelo de red, el primer paso consiste en
escoger la tecnología que dará soporte al proyecto. Las grandes
compañías de desarrollo de software tienen suficiente personal como
para implementar su propio motor de red utilizando sockets. Los
sockets ofrecen una gran flexibilidad y potencia, pero debido a que
son mecanismos de tan bajo nivel, tienen el inconveniente de obligar
al desarrollador a tener que implementar todas las características de
alto nivel que requiera como: codificación eficiente de los datos,
compresión automática de datos, ... Por ello se ha elegido utilizar
ZeroC Ice.

A continuación se irán explicando los detalles de cada uno de los
componentes que forman el modelo de red de este juego. Se comenzará
con los tipos de datos usados y el \acs{API} que usarán el cliente y
el servidor. Se seguirá explicando los detalles del servidor, para
finalizar con el cliente.

\begin{listing}[language=C++,
  caption = {Estructuras de datos usados en el modelo de red},
  label = {list:tinman-slice}]
    enum Action {AccelerateBegin, AccelerateEnd, BrakeBegin, BrakeEnd,
                                TurnRight, TurnLeft, TurnEnd, UseNitro};
    sequence<Action> ActionSeq;
    struct Quaternion {float x, y, z, w; };
    struct Vector3 {float x, y, z; };

    struct CarInfo {
      byte carId;
      int seqNumber;
      Quaternion orientation;
      Vector3 position;
      Vector3 velocity;
      ActionSeq actions;
    };

    sequence<CarInfo> Snapshot;

    struct LoginInfo {
      byte carId;
      Ice::StringSeq playerNicks;
    };

    interface Peer {
      void notify(Snapshot value);
    };

    interface AuthoritativeServer {
      void notify(Snapshot valuey);
      ["amd"] LoginInfo login(string nickname, Peer* proxy);
      ["amd"] void startRace(string nickname);
      void disconnect(byte id);
    };
\end{listing}

En el listado~\ref{list:tinman-slice} se muestran las estructuras de
datos que se usadas en las comunicaciones del juego. El tipo de datos
mas importante es \texttt{Snapshot}, que es un contenedor de
estructuras \texttt{CarInfo}. Dicha estructura representa el estado de
un coche y está formada por un identificador del jugador (usado por el
servidor), un número de secuencia de mensaje, la orientación, velocidad
y posición del coche y las acciones realizadas por el coche. En la
sección~\ref{sec:implementacion-4} se explica el tipo de acciones.

En cuanto a las interfaces, el cliente implementa la interfaz
\texttt{Peer}, cuyo único método sirve al servidor para enviar el
estado del juego actualizado a cada uno de los clientes. Por su parte,
el servidor implementa la interfaz \texttt{AuthoritativeServer}, que
cuenta con un método \texttt{notify()} homólogo al anterior, y dos
métodos \texttt{login()} y \texttt{startRace()} que son usados por los
clientes para conectarse al servidor e indicar que comience la
carrera, respectivamente.

A continuación se explicarán los detalles de implementación del
servidor.
\begin{listing}[language=C++,
  caption = {Implementación del servidor de red del juego},
  label = {list:servidor}]
class AuthoritativeServer(Tinman.AuthoritativeServer):
    peers = []
    n_players = 0
    next_race = []
    NUMBER_PLAYERS = 4

    def notify(self, snapshot, current):
        for car_info in snapshot:
            sys.stdout.flush()
            self.send_to_other_peers(car_info, snapshot)

    def send_to_other_peers(self, car_info, snapshot):
        index = 0
        for peer in self.peers:
            if index != car_info.carId:
                peer[2].notify(list(snapshot))
            index = index + 1

    def login_async(self, cb, nickname, proxy, current=None):
        print("login: {0}".format(nickname))
        if([nick for nick in self.peers if nick[1] == nickname]):
            print "Nickname {0} already registered"
            nickname = nickname + str(len(self.peers))

        self.peers.append((cb, nickname, proxy))
        if len(self.peers) == self.NUMBER_PLAYERS:
            self.start_game()

    def start_game(self):
        id = -1
        playersNick = []
        for cb, nick, proxy in self.peers:
            playersNick.append(nick)

        for cb, nick, proxy in self.peers:
            print("enviando nick jugador{0}...".format(nick))
            id = id + 1
            login_info = Tinman.LoginInfo(id, playersNick)
            cb.ice_response(login_info)

    def startRace_async(self, cb, nickname, current=None):
        self.next_race.append(cb)

        if(len(self.next_race) == self.NUMBER_PLAYERS):
            self.start_new_race()

    def start_new_race(self):
        for cb in self.next_race:
            cb.ice_response()
        self.next_race = []

    def disconnect(self, player_id, current):
        print "disconecting ", player_id, "..."
        if len(self.peers) == 1:
            del self.peers[0]
            return

        del self.peers[player_id]

class Server(Ice.Application):
    def run(self, argv):
        broker = self.communicator()
        servant = AuthoritativeServer()

        adapter = broker.createObjectAdapter("AuthoritativeServerAdapter")
        proxy = adapter.add(servant, broker.stringToIdentity("tinman"))

        print(proxy)
        sys.stdout.flush()

        adapter.activate()
        self.shutdownOnInterrupt()
        broker.waitForShutdown()

        return 0

server = Server()
sys.exit(server.main(sys.argv))
\end{listing}

El servidor está implementado en Python. La razón es que al ser un
lenguaje de prototipado el desarrollo se agilizó muchísimo, gracias a
todas las ventajas que aporta python, como ser un lenguaje
interpretado (no se compila) y tener un tipado dinámico.

Por otra parte, usar Python ayuda a ilustrar la flexibilidad que
aporta usar ZeroC Ice , que permite comunicar distintos componentes
del juego, incluso aunque estén implementados en distintos lenguajes
de programación.

La clase \texttt{Server} hereda de \texttt{Ice.Application}, clase
abstracta que proporciona \acs{ICE} para crear aplicaciones
sencillas. Dentro de la clase \texttt{Server} se inicializa el
comunicador, objeto que gestiona las operaciones de \acs{ICE}, se crea
un sirviente (objeto de la clase \texttt{AuthoritativeServerAdapter})
y se registra al adaptador de objetos. Tras esto se activa el
adaptador y se inicia el bucle principal de la aplicación usando el
método del comunicador \texttt{waitForShutdown()}.

Una vez invocado ese método el servidor quedará a la escucha en el
puerto 10000 hasta que se interrumpa la ejecución (por ejemplo,
pulsando \texttt{Ctrl + C}). La configuración del servidor se realiza
mediante línea de órdenes, pasándole la ruta de un fichero a tal
efecto. Ejecutando el comando del listado~\ref{list:run-server} se
arranca el servidor y en el listado~\ref{list:config-file-server} se
muestra el fichero de configuración. En dicho fichero se indica el
nombre del adaptador (\texttt{AuthoritativeServerAdapter}) y una lista
de endpoints, en los que se le indica el puerto y el protocolo que se
usará en dicho endpoint.

\begin{listing}[language=C++,
  caption = {Comando de ejecución del servidor del juego},
  label = {list:run-server}]
./network/server.py --Ice.Config=config/server.config
\end{listing}

\begin{listing}[language=C++,
  caption = {Fichero de configuración del servidor del juego},
  label = {list:config-file-server}]
AuthoritativeServerAdapter.Endpoints=tcp -p 10000: udp -p 10001
\end{listing}

En este proyecto se usan dos protocolos de transporte, dependiendo del
tipo de mensaje que se quiera transmitir. Las invocaciones remotas al
método \texttt{notify}, tanto de la interfaz \texttt{Peer} como de
\texttt{AuthoritativeServer} usan el protocolo \acs{UDP}. Por su
parte, las invocaciones realizadas por los clientes a los métodos
\texttt{login()} y \texttt{startRace()}, y sus correspondientes
respuestas utilizan el protocolo \acs{TCP}.

La razón de utilizar \acs{UDP} para las invocaciones que se realizan
durante el \emph{gameplay} es que \acs{TCP} no es un protocolo apropiado
para juegos de acción rápida. TCP es un protocolo orientado a flujo,
el cuál trabaja sobre IP, que es un protocolo que usa paquetes, razón
por la que debe romper el flujo de datos para enviarlo en paquetes
individuales. Debido a que TCP intenta reducir la sobrecarga de la
red, no envía paquetes mas pequeños de un determinado tamaño
(supongamos 100 bytes). Esto es un problema en juegos donde se
necesite enviar datos lo mas rápidamente posible, ya que se
almacenarán hasta que los datos almecenados superen una determinada
cantidad, momento para el que los datos pueden haberse quedado
obsoletos~\cite{tcpvsudp}.

Volviendo al servidor, en los métodos \texttt{login} y
\texttt{startRace} se utiliza \acf{AMD}(junto con su homólogo en el
lado del cliente \acf{AMI}). \acs{AMD} es un método de programación
asíncrono que permite al servidor atender varias peticiones
simultáneamente. Sin embargo, la razón por la que se usa es porque
permite a los clientes hacer login o indicar que quieren que comience
una nueva carrera sin bloquear el hilo principal de ejecución.

\begin{figure}[h]
\centering
  \includegraphics[width=1\textwidth]{ami.png}
  \caption{invocación asíncrona del cliente al servidor(\acs{AMI})}
  \label{fig:ami}
\end{figure}


\begin{figure}[h]
\centering
  \includegraphics[width=1.1\textwidth]{amd.png}
  \caption{despachado asíncrono de las peticiones del cliente por
    parte del servidor(\acs{AMD})}
  \label{fig:amd}
\end{figure}

Para explicar cómo se usa \acs{AMD} o \acs{AMI}, se mostrará el
proceso de \texttt{login}, que está ilustrado en las
figuras~\ref{fig:ami} y~\ref{fig:amd}:
\begin{itemize}
\item El cliente invoca el método \texttt{begin\_login()} (ver
  listado~\ref{list:implementacion-adaptador}. Esto devuelve un objeto
  de tipo \texttt{Ice::AsyncResultPtr} que puede ser usado para
  comprobar el estado de la transacción.
\item El servidor recibe la invocación, pero en el método
  \texttt{login\_async()}. Esto permite retrasar la respuesta de dicha
  invocación hasta el momento en el que hayan iniciado sesión un
  número mínimo de jugadores, en este caso 4 (por la variable
  \texttt{NUMBER\_PLAYERS}). Dicho método recibe tres argumentos: un
  callback que podrá invocar para informar al cliente que todos los
  jugadores se han registrado a la partida, el nickname del jugador y
  el proxy del objeto \texttt{Peer} que crea el cliente, sin el cuál
  el servidor no podría enviarle el estado de la partida, cuando esta
  comience.
\item Cuando el número de jugadores acordado se haya registrado, el
  servidor invocará a través del método \texttt{login\_async()} a
  \texttt{start\_game()}. Usando los callbacks (\texttt{cb}) recibidos
  antes, el servidor responde a todos los clientes.

\begin{listing}[language=C++,
  caption = {Método update de la clase Menu},
  label = {list:menu-update}]
void
Menu::update(float delta) {
  game_->gui_->inject_delta(delta);
  expose_car_->animation(delta);
  if(login_ && login_->isCompleted()) {
    complete_login();
  }
}
\end{listing}

\item Por último, el método \texttt{start\_game()} del sirviente
  \texttt{AuthoritativeServer} invocará el método del
  callback\texttt{ice\_response}.
\item El cliente, a través del método \texttt{isCompleted()} será
  notificado de que el proceso de login ha finalizado, y cambiará al
  estado \texttt{Play}, donde comenzará la carrera.
\end{itemize}

\begin{listing}[language=C++,
  caption = {Implementación del módulo de red del cliente},
  label = {list:implementacion-adaptador}]
Adapter::Adapter(Car::shared car) {
  try {
    seq_number_ = 0;
    socket_ = {"1", 10000};
    car_ = car;
    Ice::StringSeq argv{"Tinman"};
    communicator_ = Ice::initialize(argv);
  }catch(const ::Ice::LocalException& exception) {
    std::cerr << "No server running" << std::endl;
  }
}


Ice::AsyncResultPtr
Adapter::login(std::shared_ptr<Session> session) {
  try{
    if(peer_)
      return auth_server_->begin_login(session->human_player_->nick_, peer_);

    std::stringstream endpoint;
    endpoint << "tcp:udp";

    endpoint.str("");
    endpoint << "tinman: tcp -h " << socket_.first << " -p "<< socket_.second;
    endpoint << ": udp -h " << socket_.first << " -p "<< socket_.second + 1;
    endpoint.str();

    ObjectPrx proxy = communicator_->stringToProxy(endpoint.str());

    auth_server_ = Tinman::AuthoritativeServerPrx::checkedCast(proxy);
    dgram_auth_server_ = Tinman::AuthoritativeServerPrx::uncheckedCast(proxy->ice_datagram());

    Tinman::PeerPtr servant = new Tinman::PeerI(session);
    ObjectAdapterPtr adapter;
    adapter = communicator_->createObjectAdapterWithEndpoints("PeerAdapter", "tcp:udp");

    ObjectPrx peer_proxy = adapter->add(servant,
                                        communicator_->stringToIdentity("peer-tinman"));


    peer_ = Tinman::PeerPrx::uncheckedCast(peer_proxy->ice_datagram());
    std::cout << peer_proxy << std::endl;
    adapter->activate();

    return auth_server_->begin_login(session->human_player_->nick_, peer_);
  }catch(const ::IceUtil::NullHandleException& exception) {
    std::cerr << "Error: login failed!" << std::endl;
    return nullptr;
  }
  catch(const Ice::ConnectionRefusedException exception) {
    std::cerr << "Error: connection refused!" << std::endl;
    return nullptr;
  }
}

Tinman::LoginInfo
Adapter::complete_login(Ice::AsyncResultPtr async_result) {
  login_info_ = auth_server_->end_login(async_result);
  return login_info_;
}

Ice::AsyncResultPtr
Adapter::next_race(std::string nickname) {
  return auth_server_->begin_startRace(nickname);
}

Ice::AsyncResultPtr
Adapter::next_race(std::string nickname) {
  return auth_server_->begin_startRace(nickname);
}

void
Adapter::notify(Tinman::Action action) {
  Tinman::Vector3 position{car_->get_position().getX(),
      car_->get_position().getY(), car_->get_position().getZ()};

  Tinman::Vector3 velocity{car_->get_velocity().getX(),
      car_->get_velocity().getY(), car_->get_velocity().getZ()};

  Tinman::Quaternion orientation{car_->get_orientation().getX(),
      car_->get_orientation().getY(),
      car_->get_orientation().getZ(),
      car_->get_orientation().getW()};

  Tinman::CarInfo car_state;
  car_state.carId = login_info_.carId;
  car_state.seqNumber = ++seq_number_;
  car_state.orientation = orientation;
  car_state.position = position;
  car_state.velocity = velocity;
  car_state.actions = Tinman::ActionSeq{action};

  Tinman::Snapshot snapshot = {car_state};

  dgram_auth_server_->notify(snapshot);
}

void
Adapter::disconnect() {
  std::cout << "[Adapter] " << __func__ << std::endl;
  dgram_auth_server_->disconnect(login_info_.carId);
}

\end{listing}

Como se ve, aunque el proceso de invocación asíncrono en el cliente y
de despachado asíncrono en el servidor es complejo, permite que no se
bloquee la interfaz del cliente cuando realiza dicha invocación,
mientras que el despachado asíncrono permite al servidor recibir
múltiples peticiones simultáneamente.

Por último, hay que mencionar que se extendió la funcionalidad del
jugador creando una clase asbtracta llamada \texttt{Controller}. Dicha clase
permite crear diferentes tipos de jugadores, descoplando la lógica de
control del jugador de la clase \texttt{Player}. En la
fig~\ref{fig:jerarquia-controladores} se puede observar la jerarquía
de controladores.

En esta iteración se crearon dos tipos de controladores:
\texttt{Human} que representa al jugador humano y \texttt{Network},
que representa a los jugadores remotos. Se llama jugador local al
jugador humano, y en contrapunto, jugador remoto a los jugadores
humanos que juegan en otros ordenadores dentro de la misma partida.

En los listados~\texttt{list:controller}, \ref{list:human}
y~\ref{list:network} se observan la clase padre y la
implementación de las clases \texttt{Human} y \texttt{Network}
respectivamente.

\begin{listing}[language=C++,
  caption = {Declaración clase Controller},
  label = {list:controller}]
class Controller {
 public:
  std::queue<Tinman::CarInfo> updates_;
  Car::shared car_;
  ControllerObserver::shared observer_;

  typedef std::shared_ptr<Controller> shared;

  Controller(Car::shared car);
  virtual ~Controller();

  virtual void configure() = 0;
  virtual void update(float delta) = 0;
  virtual void exec(Tinman::Action action) = 0;

  void disconnect();
};
\end{listing}


\begin{listing}[language=C++,
  caption = {clase NetworkController},
  label = {list:network}]
HumanController::HumanController(EventListener::shared listener,
                        KeyBinding key_bindings, Car::shared car): Controller(car) {
  input_ = listener;
  key_bindings_ = key_bindings;
  network_mode_ = true;
}
void
HumanController::configure() {
  std::cout << "[HumanController] configure" << std::endl;
  for(auto key_bind: key_bindings_) {
    input_->add_hook({key_bind.first, EventTrigger::OnKeyPressed}, EventType::repeat,
                     std::bind((void(HumanController::*)
                     (Tinman::Action))(&HumanController::exec), this, key_bind.second.first ));
    input_->add_hook({key_bind.first, EventTrigger::OnKeyReleased}, EventType::doItOnce,
                     std::bind((void(HumanController::*)
                     (Tinman::Action))(&HumanController::exec), this, key_bind.second.second));
  }
}

void
HumanController::update(float delta) {
  car_->update(delta);
}

void
HumanController::exec(Tinman::Action action) {
  car_->exec(action);
  if(network_mode_)
    observer_->notify(action);
}
\end{listing}

\begin{listing}[language=C++,
  caption = {clase HumanController},
  label = {list:human}]


NetworkController::NetworkController(Car::shared car):  Controller(car) {
  duration_ = last_size_ = 0.f;
  last_num_sequence_ = -1;
}

void
NetworkController::configure() {
}

void
NetworkController::exec(Tinman::Action action) {
    car_->exec(action);
}

void
NetworkController::update(float delta) {
  for(int snapshots_per_frame = get_snapshots_per_frame(delta);
      snapshots_per_frame != 0;
      snapshots_per_frame--) {

    if(updates_.size() > 1 &&
       updates_.front().seqNumber > last_num_sequence_) {

      for(auto action: updates_.front().actions)
        car_->exec(action);
      car_->synchronize(updates_.front());
      last_num_sequence_ = updates_.front().seqNumber;
      updates_.pop();
    }
  }

  car_->update(delta);
}

int
NetworkController::get_snapshots_per_frame(float delta) {
  if(updates_.empty()) {
    int snapshots_initial_case = 0;
    return snapshots_initial_case;
  }

  if(updates_.size() <= 6) {
    return 1;
  }

  int snapshots_per_frame = updates_.size() - 6;
  return snapshots_per_frame;
}
\end{listing}
\begin{listing}[language=C++,
  caption = {clase PeerI},
  label = {list:peer}]
Tinman::PeerI::PeerI(Session::shared session) {
  session_ = session;
}



void
Tinman::PeerI::notify(const ::Tinman::Snapshot& snapshot,
                      const Ice::Current& current) {
  session_->store(snapshot);
}
\end{listing}
\begin{listing}[language=C++,
  caption = {Metodo store de la clase Session},
  label = {list:session-store}]
void
Session::store(Tinman::Snapshot snapshot) {
  for(auto car_info: snapshot)
    get_player(car_info.carId)->store(std::move(car_info));
}
\end{listing}
En el método \texttt{exec()} de la clase \texttt{HumanController} se
notifica al servidor el estado del coche en cada momento. En el método
\texttt{notify} de la clase \texttt{Adapter} se observa la
construcción del mensaje que se envía al servidor (ver
listado~\ref{list:implementacion-adaptador}).

Por otra parte, el método \texttt{update} de la clase
\texttt{NetworkController} se utiliza para sincronizar el estado de
los jugadores remotos a partir de los mensajes recibidos por parte del
servidor. En el listado~\ref{list:peer} se puede ver el código que se
ejecuta cuando el cliente recibe una invocación del servidor, mientras
que en el método \texttt{notify} del servidor (ver
listado~\ref{list:servidor}) se observa cómo notifica de los cambios
en el estado del juego el servidor a los demás clientes.

Si se observa la clase \texttt{PeerI} (listado~\ref{list:peer}) se
puede ver lo genérico que es el proceso de notificación. El servidor
invoca el método \texttt{notiy} del proxy del cliente, el cliente
recibe la invoca en dicho método y se invoca el método \texttt{store}
de la clase \texttt{Session}, que almacena en un buffer los snapshots
recibidos por le servidor (ver listado~\ref{list:session-store}. En
dicho método se comprueba a que jugador remoto va dirigido cada uno de
los snapshots recibidos y se almacena en el atributo de tipo
\texttt{Adapter} que almacena \texttt{NetworkController}.

\subsection{Análisis del ancho de banda consumido}
\label{sec:analisis-del-ancho}
Se ha realizado una estimación del ancho de banda consumido por los
clientes por el protocolo de red usado en este proyecto. Esta
estimación está basada en el tamaño de cada mensaje multiplicado por
el número de mensajes enviados por segundo.

El tamaño de cada mensaje es el siguiente:
\begin{itemize}
\item Cada mensaje contiene una estructura de tipo \texttt{CarInfo}.
\item Cada estructura \texttt{CarInfo} contiene:
  \begin{itemize}
  \item un byte (1 byte).
  \item un entero (4 bytes).
  \item la posición, que es un vector de tres floats ( 3 * 4 bytes).
  \item la rotación, que es un vector de cuatro floats ( 4 * 4 bytes).
  \item la velocidad, que es un vector de tres floats (3 * 4 bytes).
  \item una secuencia de acciones, que típicamente contiene una única
    acción. Dado que el tipo de datos \texttt{Action} es un enumerado,
    típicamente el compilador lo convierte a tipo entero (4 byte).
  \end{itemize}
\end{itemize}

Por otra parte, la cabecera típica de un mensaje de ZeroC \acs{ICE}
ocupa 14 bytes\footnote{Información del protocolo de \acs{ICE}:
  \url{https://doc.zeroc.com/display/Ice35/Protocol+Messages}}.

En total, el tamaño del mensaje es $1 + 4 + 12 + 16 + 12 + 4 = 49
bytes$. Dado que el cliente envía 60 mensajes por segundo al servidor,
el cliente consume un total de $60 · (49 + 14) = 3780 bytes/segundo = 3,7 Kbytes/segundo$

Por su parte, el servidor consume ese mismo ancho de banda, pero
multiplicado por cada jugador, de forma que para este videojuego
consumiría
$4 jugadores · 3,7 Kbyte/segundo por jugador = 14,8 Kbytes /segundo$

Por tanto, el ancho de banda total consumido por el protocolo del
juego es $14.8 Kbytes / segundo$.
% Local Variables:
% coding: utf-8
% fill-column: 90
% mode: flyspell
% ispell-local-dictionary: "american"
% mode: latex
% TeX-master: "main"
% End:

%  LocalWords:  callbacks sockets middleware
