Esta iteración busca solucionar el problema relativo a la creación de circuitos
para el juego. Para facilitar la labor de diseño, se crearán circuitos muy
parecidos a los existentes en el juego original, \emph{Super Off Road}. Se
pretende crear circuitos de una forma lo mas sencilla posible, tratando de
reducir al mínimo el trabajo de modelado al tiempo que se hace mas simple la
adición de nuevos circuitos en un futuro.

\subsection{Análisis}
\label{sec:analisis-6}

El objetivo que se persigue en esta iteración es el de crear circuitos sobre los
que disputar las carreras del juego. Dado que se pretende crear un clon del
juego \emph{Super off Road}, la estética de los circuitos de este videojuego
estarán inspirados en los de aquél. En la figura~\ref{fig:superOffRoadTrack} se
muestra uno de los circuitos originales de dicho juego.

\begin{figure}[h]
  \centering
  \includegraphics[width=10cm]{super-off-road-track.png}
  \caption[Circuito del juego \emph{Super Off Road}]{Uno de los circuitos del clásico arcade de carreras \emph{Super Off Road}\protect\footnotemark}
  \label{fig:superOffRoadTrack}
\end{figure}
\footnotetext{\url{http://i.ytimg.com/vi/3vXfvHDOfqQ/maxresdefault.jpg}} Por lo
tanto, el primer paso consiste en analizar en detalle los circuitos
originales. Estos se caracterizan por tener distintos tipos de terreno: rampas
ascendentes y descendentes, badenes, con charcos, bacheados, etcétera. Además,
si se observan detenidamente se puede ver cómo en realidad estos circuitos no
dejan de ser una sucesión de los mencionados segmentos de terreno, dispuestos en
una rejilla de 5x5. Debido a que el juego original tenía que lidiar con unos
recursos hardware muy limitados, quedaba descartado utilizar gráficos
tridimensionales, de forma que los creadores tuvieron que simular los saltos,
las subidas y bajadas de los coches jugando con la perspectiva. Por otra parte,
los circuitos no son regulares debido a que tenían que caber en las reducidas
resoluciones de pantalla existentes en la época\footnote{La fecha de lanzamiento
  del juego \emph{Super Off Road} fue en 1989}, razón por la que es complicado
ver que en realidad son piezas similares puestas en sucesión.

A la hora de crear los circuitos entra en juego lo que se conoce como
\emph{modelado 3D}. Básicamente, este proceso consiste en moldear a un cuerpo
convexo hasta que se alcanza la forma deseada. Este es un proceso lento y
costoso, que requiere de una larga experiencia para obtener un resultado de una
mínima calidad. Por tanto, se ha de intentar reducir al mínimo el coste de este
trabajo. Normalmente en la creación de videojuegos, los escenarios se crean en
un programa de creación de gráficos 3D, en el caso de este proyecto
\emph{Blender}. En un primer lugar se crean los elementos individuales que
conformarán el escenario, para después ser colocados de forma que se crea la
escena deseada, que será exportada a un formato con el que el motor de renderizado
usado por el videojuego pueda trabajar.

Crear un formato textual para representar los circuitos formados por segmentos
tiene como ventaja que es mas sencillo añadir nuevos, ya que todo se reduce a
crear un nuevo fichero.

\subsection{Diseño e Implementación}
\label{sec:implementacion-6}


A la hora de crear los segmentos que formarán el circuito, se han tenido en
cuenta una serie de consideraciones:
\begin{itemize}
\item En primer lugar, tienen que ser complementamente simétricos, bien por la
  mitad en el caso de las rectas o por la diagonal en el caso de las
  curvas. Esto es asi debido a que a la hora de rotarlos si no son simétricos no
  encajan bien unos con otros.
\item Aunque los segmentos cuentan con una valla, ésta no es lo suficientemente
  alta como para evitar que los vehículos la salten. Para eliminar este problema
  se ha tenido que añadir una pared transparente que haga de barrera. En el
  juego original no existía este problema debido a que era un juego en 2D, y el
  motor de físicas no podía modelar saltos.
\end{itemize}

Siguiendo estas pautas, se han modelado los segmentos del circuito usando
Blender. En la figura~\ref{fig:modelado-segmentos} se muestra una captura de
pantalla de Blender mostrando algunas de las piezas que se han modelado. En
total se han creado inicialmente 4 modelos: uno para las \emph{rectas}, otro
para la \emph{meta}, que se diferencia del anterior en que tiene una raya a
cuadros en la mitad indicando la línea de salida, otro para las \emph{curvas},
de los cuales se exportaron cuatro, correspondientes a cada uno de los
cuadrantes circulares y otro para las \emph{rampas}, que permitirán a los coches
dar saltos, lo que añade algo de dinamismo (no aparece en la figura debido a que
es muy parecido estéticamente a la recta).


\begin{figure}[h]
\centering
\includegraphics[width=0.8\textwidth]{creacion-circuito.png}
  \caption{Modelado de los segmentos que formarán los circuitos}
  \label{fig:modelado-segmentos}
\end{figure}

Una vez modelados los segmentos\footnote{se puede ver el proceso de creación de
  una curva en \url{https://goo.gl/exFdSL}}, el siguiente paso consiste en
codificar los circuitos de tal forma que se puedan representar en un fichero de
texto. Para ello se decidió utilizar una serie de caracteres unicode, que
representan los diferentes tipos de segmentos. Combinando los caracteres en
diferentes combinaciones se pueden crear circuitos de una forma muy visual y
rápida. Esto facilita la creación de circuitos especiales para crear tests
específicos, ya que crear un nuevo circuito es muy sencillo. En la
figura~\ref{fig:circuito-ejemplo} se puede ver un listado de los caracteres
usados, junto con un ejemplo de circuito.

\begin{figure}[h]
\centering
\includegraphics[width=0.6\textwidth]{unicode-track.png}
\includegraphics[width=0.8\textwidth]{circuito-sample.png}
  \caption[Fichero de definición de un circuito]
  {Ejemplo de fichero de definición circuito con el circuito que se crea a
    partir de él}
  \label{fig:circuito-ejemplo}
\end{figure}

Una vez establecida la estructura del fichero de definición de circuitos, el
siguiente paso consiste en crear una clase para encapsular la funcionalidad
relacionada con la creación de circuitos. Para ello, se ha creado la clase
Track, cuya declaración se puede ver en el listado~\ref{list:declaracion-track}
y la implementación de los métodos mas relevantes
en~\ref{list:implementacion-track}.Esta clase representa un circuito, el cuál
está formado por un vector de secciones. Cada uno de las variables que la forman
son:
\begin{itemize}
\item\texttt{Type}: El tipo de sección. Esta información será usada
  posteriormente por el algoritmo de inteligencia artificial.
\item\texttt{node}: Un nodo de Ogre, que será usado para renderizar ese segmento de circuito.
\item\texttt{physic\_entity}: Una entidad que guarda la malla 3D de la Sección.
\item\texttt{body}: Un cuerpo físico, que gracias a las clases btBvhTriangleMeshShape y
  MeshStrider, tendrá la misma forma geométrica que la malla 3D usada por
  Ogre, lo que permite que el coche interaccione con un gran realismo. La
  primera clase crea una forma de colisión estática creada con mallas
  triangulares. La segunda es una clase que coge la geometría de la malla de la
  entidad de Ogre3D con la intención de crear la forma de colisión anterior.
\item\texttt{position}: La posición en coordenadas globales del segmento.
\item\texttt{id} Un identificador.
\end{itemize}
\begin{listing}[language=C++,
  caption = {Declaración de la clase Track},
  label = {list:declaracion-track}]
  struct Section {
    enum Type{Meta, Rect1, Rect2, RightCurve1,
      RightCurve2, LeftCurve1, LeftCurve2};
    Type type;
    Ogre::SceneNode* node;
    Ogre::Entity* physics_entity;
    btRigidBody* body;
    btVector3 position;
    int id;
  };

  class Track {
    std::string file_name_;
    std::wifstream file_;
    std::map<wchar_t, std::function<Section()>> builder_;
    std::map<wchar_t, std::pair<char, char>> directions_;
    std::map<char, char> ends_;

    int x_, z_, index_;
    Scene::shared scene_;
    Physics::shared physics_;
    wchar_t current_char_;
    std::vector<Ogre::SceneNode*> stairs_;
    public:
    typedef std::shared_ptr<Track> shared;
    std::vector<Section> segments_;
    int columns_;

    Track(std::string file);

    void next(std::string file);
    void next(Scene::shared scene, Physics::shared physics);
    Section create_section(std::string name);

    void reset();
  };
\end{listing}
\begin{listing}[language=C++,
  caption = {Implementación de algunos de los métodos mas importantes de la clase Track},
  label = {list:implementacion-track}]
Track::Track(std::string file) {
  file_name_ = file; x_ = columns_ = 0; z_ = -60;

  builder_[u'\u2500'] = std::bind(&Track::create_section, this, "rect1");
  builder_[u'\u2502'] = std::bind(&Track::create_section, this, "rect2");
  builder_[u'\u256D'] = std::bind(&Track::create_section, this, "curve-right2");
  builder_[u'\u2570'] = std::bind(&Track::create_section, this, "curve-right1");
  builder_[u'\u256E'] = std::bind(&Track::create_section, this, "curve-left2");
  builder_[u'\u256F'] = std::bind(&Track::create_section, this, "curve-left1");
  builder_[u'\u257E'] = std::bind(&Track::create_section, this, "ramp1");
  builder_[u'\u257F'] = std::bind(&Track::create_section, this, "ramp2");
  builder_[u'\u2505'] = std::bind(&Track::create_section, this, "meta");
}

void
Track::open_file(std::string file) {
  file_.open(file);
  file_.imbue(std::locale(""));
  std::cout << "open file: " << file << std::endl;
  if (!file_.is_open()) {
    std::cerr << "no file: \n"
              << file
              << std::endl;
    exit(-1);
  }
}

void
Track::next(std::string file) {
  index_ = 0;
  open_file(file);

  std::wstring line;
  while(getline(file_, line)) {
    for(auto circuit_segment: line){
      if(builder_[circuit_segment]){
        current_char_ = circuit_segment;
        segments_.push_back(builder_[circuit_segment]());
      }
      x_ += 20;
      index_++;
    }
    z_ += 20;
    columns_ = x_ / 20;
    x_ = 0;
  }
  z_ = -60;
  file_.close();
  order_track();
}

void
Track::order_track() {
  auto meta = std::find_if(segments_.begin(), segments_.end(), [&](auto segment){
    return segment.type == Section::Type::Meta;
  });
  int id = meta->id;
  meta->id = 0;
  char direction = '\0';
  int last_id = id;
  char last_direction = 'L';
  for(int index = 1; index < segments_.size(); index++) {
    direction = get_direction(segments_[id].type, segments_[last_id].type, last_direction);
    last_direction = direction;
    last_id = id;
    id = get_next_section(id, direction);

    segments_[id].id = index;
  }
  std::sort(segments_.begin(), segments_.end(), [&](auto segment1, auto segment2){
    return segment1.id < segment2.id;
  });
}

void
Track::reset() {
  for(auto section: segments_) {
    scene_->destroy_node(section.node);
    scene_->destroy_entity(section.physics_entity);
    physics_->remove_rigid_body(section.body);

    delete section.body->getCollisionShape();
    delete section.body;
  }
  segments_.clear();

  for(auto node: stairs_)
    scene_->destroy_node(node);
  }
\end{listing}

El algoritmo de creación del circuito se puede ver en el
listado~\ref{list:implementacion-track}, en el método \texttt{next()}. A pesar de su sencillez, cabe destacar un par de aspectos del
listado anterior:
\begin{itemize}
\item En primer lugar se abre el fichero del circuito utilizando el método
  \texttt{open\_file(file)}. Debido a que se está trabajando con caracteres
  unicode, es necesario especificar la codificación que se va a utilizar. En el
  caso de este proyecto, se usan los locales de C. Esto usando el método
  \texttt{imbue()} de la clase de la clase \emph{ifstream}, que se encarga de
  encapsular las operaciones relacionadas con la lectura de ficheros.
\item En el constructor de \texttt{Track} se inicializa un mapa que relaciona
  caractéres unicode con callbacks al método \texttt{create\_section()}. Dicha
  función crea el cuerpo gráfico y el físico de la sección haciendo uso de la
  clase \emph{Scene}, que es el wrapper de Ogre3D, y de la clase \emph{Physics},
  que es el wrapper de Bullet Physics\footnote{ambas clases han sido
    implementadas en este proyecto}.
\item Dentro del \texttt{while} se recorre el fichero línea por línea y dentro
  del for se recorre cada línea carácter por carácter. Por cada carácter, se
  crea una nueva sección en la posición que le corresponda. Para ello, existen
  dos variables que sirven como desplazamiento en el eje X y Z, que se
  incrementan de 20 en 20, pues esa es el tamaño de los segmentos (20 unidades
  de largo y 20 de ancho).
\item Cada uno de los segmentos se almacenan en el vector \texttt{segments\_},
  para poder acceder a ellos mas adelante.
\item Tras terminar de crear los segmentos del circuito todavía queda un último
  paso por hacer, ordenar el buffer \texttt{segments\_} para que se pueda
  acceder a los segmentos en orden. Es decir, el orden deseable es comenzando
  desde la meta, siguiendo la orientación de los segmentos, hasta llegar al
  segmento anterior a esta.

  Esto mismo es lo que hace el método \texttt{order\_track()}: busca el segmento
  de tipo \texttt{Meta} dentro del buffer. En este momento se usará la variable
  \texttt{id} para ordenar en orden ascendente el vector. Por tanto, se le
  asigna el id 0 al segmento \texttt{Meta } y, suponiendo que el sentido del
  circuito es hacia la izquierda, se recorren cada uno de los siguientes
  segmentos hasta llegar al final, asignándoles un valor creciente a la variable
  id de cada uno de ellos.
\item Por último, se usa el algoritmo \texttt{sort} de la biblioteca estándar
  para ordenar los segmentos en orden creciente atendiendo al valor del atributo
  \texttt{id}.
\end{itemize}

El resultado de esta iteración se puede observar en la
figura~\ref{fig:iteration6}. El circuito que se muestra en la
figura~\ref{fig:circuito-ejemplo} corresponde a la fase final del proyecto.

\begin{figure}[h]
  \centering
  \includegraphics[width=14cm]{iteration6.png}
  \caption[Resultado de la iteración 7]{Resultado de la iteración 7. Primer circuito creado}
  \label{fig:iteration6}
\end{figure}
