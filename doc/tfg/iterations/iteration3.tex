En esta iteración se pretende implementar la lógica necesaria para que el
vehículo creado en la iteración anterior pueda ser controlado por un jugador
usando el teclado. Para
conseguir esto se implementará el soporte necesario para detectar qué teclas son
pulsadas y ejecutar alguna acción en respuesta.

\subsection{Análisis}
\label{sec:analisis-3}
El objetivo es conseguir que el coche renderizado en la iteración anterior pueda
ser movido por el usuario. Para ello, se hace necesario el uso de alguna
biblioteca que ayude a gestionar los eventos de teclado y de ratón.

Por otra parte, en cualquier videojuego, normalmente se asignan una serie de
teclas, bien de teclado o bien de un joystick, a una serie de acciones del
juego; en nuestro caso, podría ser acelerar o frenar el coche, hacerlo girar,
etcétera. En algún punto del programa habrá que comprobar qué teclas se han
pulsado y, en respuesta, ejecutar la acción asociada. Por esta razón, sin
mecanismos de alto nivel que añadan flexibilidad a la hora de manejar los
eventos, la solución a este problema pasa por crear una función, que será
invocada una vez en cada iteración del bucle principal del juego y contendrá una
sentencia condicional \emph{if} por cada acción que pueda realizar el
jugador. Dichas sentencias condicionales contendrán el código asociado a una
determinada tecla. Esta solución no es flexible ni mantenible ya que se acopla
toda la lógica de control a un único fragmento de código, de forma que en el
momento que se quiera extender o modificar será complicado eliminar las
dependencias que aparecerían en esta situación. Por esta razón se ha de buscar
una implementación alternativa que permita solucionar dicho problema.

\subsection{Diseño e Implementación}
\label{sec:implementacion-3}

A la hora de elegir una biblioteca de gestión de eventos de entrada, la elección
en este proyecto ha sido \acf{OIS}~\cite{OIS-repo}. La razón de la elección
radica en la sencillez de integración junto a nuestro motor de juego y el hecho
de ser multiplataforma, teniendo soporte para Windows, \acs{GNU}/Linux y
Android. En cuanto a la funcionalidad ofrecida, \acs{OIS} permite gestionar
eventos de teclado, tanto de pulsación como de liberación de teclas, y eventos
de ratón: permite gestionar hasta siete teclas de ratón (con sus
correspondientes eventos de liberación y pulsado) así como eventos de
movimiento. Además ofrece soporte para Joystick y mando. Debido a todas estas
razones \acs{OIS} cubre todas las necesidades técnicas que se tenían. La
biblioteca SDL tambien ofrece el mismo soporte, pero se optó por elegir
\acs{OIS} debido a la sencillez a la hora de integrarla con el motor de juego.

Una vez elegida la tecnología sobre la que apoyar el desarrollo, el siguiente
paso consiste en implementar una clase que permita gestionar de forma flexible
eventos de teclados, así como mapear teclas a callbacks.

Estudiando detenidamente el problema se observa que se puede resolver utilizando
el patrón de diseño \emph{Reactor}. Según~\cite{Buschmann:1996:PSA:249013}, este
patrón permite a aplicaciones dirigidas por eventos despachar múltiples
peticiones destinadas a una aplicación procedentes de una o mas fuentes de
datos. Tradicionalmente, este ha sido un patrón usado en comunicaciones de red
para resolver los problemas de asincronismo propios de este contexto y así poder
realizar otras acciones hasta que se recibiesen los mensajes que se esperan. En
el caso de este proyecto, si se realizase una espera síncrona para gestionar los
eventos de teclado generados por el usuario no se podrían realizar otras
acciones. Por otra parte, en nuestro caso sólo existe una fuente de datos, el
dispositivo que esté usando el jugador, pero existen múltiples tipos de eventos
en los que se está interesado y así como de acciones acciones asociadas a dichos
eventos, de forma que este patrón encaja bien para resolver el problema que se
está estudiando.

\begin{listing}[language=C++,
    caption = {Declaración de la clase EventListener},
    label = {list:declaracion-input}]
enum class EventType{Repeat, DoItOnce};
enum class EventTrigger{ OnKeyPressed, OnKeyReleased};

class EventListener: public Ogre::WindowEventListener,
  public OIS::KeyListener,
  public OIS::MouseListener {

    typedef bool OnKeyPressed;
  typedef std::pair<OIS::MouseButtonID, EventTrigger> MouseKey;
  typedef std::pair<OIS::KeyCode, EventTrigger> KeyBoardKey;

  OIS::InputManager* input_manager_;
  OIS::Mouse* mouse_;
  OIS::Keyboard* keyboard_;

  MouseKey mouse_key_;

  std::map<MouseKey, std::function<void()>> mouse_triggers_;

 public:
  typedef std::shared_ptr<EventListener> shared;
  typedef std::vector<KeyBoardKey> KeyEvents;

  float x, y;
  bool exit_;

  EventListener(Ogre::RenderWindow* window);

  void capture(void);
  void check_events();

  void add_hook(MouseKey key,std::function<void()> callback);
  void add_hook(KeyBoardKey keystroke, EventType type,
                             std::function<void()> callback);
  void clear_hooks();

  bool keyPressed(const OIS::KeyEvent& arg);
  bool keyReleased(const OIS::KeyEvent& arg);
  bool mouseMoved(const OIS::MouseEvent&  evt);
  bool mousePressed(const OIS::MouseEvent& evt,
                          OIS::MouseButtonID id);
  bool mouseReleased(const OIS::MouseEvent& evt,
                           OIS::MouseButtonID id);
  ...
  };
\end{listing}
\begin{listing}[language=C++,
    caption = {Métodos mas importantes de la clase EventListener},
    label = {list:implementacion-input}]
void
EventListener::add_hook(EventListener::KeyBoardKey keystroke,
              EventType type, std::function<void()> callback) {
  if(type == EventType::repeat && !repeat_triggers_[keystroke])
      repeat_triggers_[keystroke] = callback;
  else if(type == EventType::doItOnce && !doitonce_triggers_[keystroke]) {
      doitonce_triggers_[keystroke] = callback;
  }
}

void
EventListener::check_events(void) {
  if(mouse_key_.first != OIS::MB_Button7)
    trigger_mouse_events();
  if(!events_.empty())
    trigger_keyboard_events();
}

void
EventListener::trigger_keyboard_events() {
  if(events_.size() > doitonce_triggers_.size() + repeat_triggers_.size())
    return;

  for(auto event: events_){
    if(doitonce_triggers_[event]){
      doitonce_triggers_[event]();
      remove_key_from_buffer(event);
    }

    if(repeat_triggers_[event])
      repeat_triggers_[event]();
  }
}

bool
EventListener::keyPressed(const OIS::KeyEvent& arg) {
  CEGUI::GUIContext& context = CEGUI::System::getSingleton().
    getDefaultGUIContext();
  context.injectKeyDown((CEGUI::Key::Scan) arg.key);
  context.injectChar(arg.text);

  remove_key_from_buffer({arg.key, EventTrigger::OnKeyReleased});
  events_.push_back({arg.key, EventTrigger::OnKeyPressed});
  return true;
}

void
EventListener::remove_key_from_buffer(KeyBoardKey event) {
    auto keyevent = find (events_.begin(), events_.end(), event);
    if(keyevent == events_.end())
      return;
    events_.erase(keyevent);
}
\end{listing}

A continuación se tratará de explicar la idea general del gestor eventos. En el
listado~\ref{list:declaracion-input} se muestra la declaración de la clase, así
como los tipos de datos utilizados. En el
listado~\ref{list:implementacion-input} se muestra la implementación de los
métodos mas importantes:
\begin{itemize}
\item En la inicialización del videojuego, se mapea una serie de teclas de
  teclado con acciones del juego. En el listado~\ref{list:mapeo-accion-callback}
  se puede ver un ejemplo de cómo asociar la tecla \texttt{Escape} con el método
  \texttt{shutdown()} de la clase \texttt{Game}.
  \begin{listing}[language = C++,
    caption = {Ejemplo de mapeo tecla-acción},
    label = list:mapeo-accion-callback]
    game_->input_->add_hook({OIS::KC_ESCAPE,
                           EventTrigger::OnKeyPressed},
                           EventType::doItOnce,
                           std::bind(&Game::shutdown, game_));
  \end{listing}
  En dicho listado se invoca al método \texttt{add\_hook} de la clase
  \texttt{EventListener}. El primer argumento es un par que contiene el código
  asociado a la tecla pulsada y un tipo enumerado llamado
  \texttt{EventTrigger}. Dicho tipo de datos indica la condición bajo la que se
  ejecutará la acción asociada, y tiene dos posibles valores:
  \begin{itemize}
  \item \texttt{OnKeyPressed}: el evento se lanzará cuando la tecla asociada sea
    presionada.
  \item \texttt{OnKeyReleased}: el evento se lanzará cuando la tecla asociada
    sea liberada.
  \end{itemize}
  El segundo argumento es de tipo \texttt{EventType} e indica la cantidad de
  veces que se debe ejecutar la acción asociada:
  \begin{itemize}
  \item \texttt{Repeat}: la acción se ejecutará mientras se cumpla la condición
    de activación del evento. Si la condición es de pulsado, se ejecutará
    mientras la tecla esté pulsada y si es de liberado, mientras la tecla se
    encuentre liberada.
  \item \texttt{DoItOnce}: la acción se ejecutará una vez en cada ocasión que se
    cumpla la condición de lanzamiento del evento.
  \end{itemize}
  Esta característica es muy importante ya que, por ejemplo, en un juego de
  coches el jugador esperará poder acelerar mientras tenga la tecla de acelerar
  pulsada y, en cambio, cuando pulse un botón de un menú esperará que las
  acciones ejecutadas se reproduzcan una única vez.

  El tercer y último argumento es un \texttt{functor}. Un \texttt{functor} es un
  objeto que implementa el \texttt{operador()}; es decir, que es invocable. En
  el ejemplo, se hace uso de la función \texttt{bind()} de la biblioteca
  estándar para crear el puntero a la función \texttt{shutdown()} de la clase
  \texttt{Game}.

  La función \texttt{bind()} hace muy sencillo crear un functor de una función
  miembro. Existe una diferencia fundamental entre invocar una función e invocar
  un método (función miembro) de una clase: el método tiene asociada una
  instancia concreta de dicha clase. Ya que los objetos tienen un estado
  asociado, para poder crear un puntero a función miembro es necesario poder
  asociar la información contextual de la instancia con dicho puntero. Dicha
  información hace posible que mas adelante, cuando se quiera usar dicho
  functor, sea posible recuperar el estado de la instancia concreta. Si no se
  asociase la información contextual de la instancia con el functor, no se
  sabría a qué instancia se está haciendo referencia, con lo que la invocación
  al método no tendría sentido.

\item Durante la ejecución del juego, cada vez que el jugador pulse o libere una
  tecla, tanto de teclado como de ratón, se invocarán a los métodos
  \texttt{keyPressed, keyReleased, mouseMoved, mousePressed}. En el
  listado~\ref{list:implementacion-input} sólo se muestra la implementación del
  método \texttt{keyPressed} por brevedad, pero los otros métodos son
  parecidos. Por tanto, se va a explicar únicamente el funcionamiento del método
  \texttt{keyPressed}.

  Este método es invocado por el objeto \texttt{OIS::InputManager} cada vez que
  se pulsa una tecla (declarado en el listado~\ref{list:declaracion-input}) y
  recibe como argumentos la información relativa a la tecla que ha sido
  pulsada. Acto seguido, se traduce el nombre de la tecla a un formato que pueda
  entender CEGUI (biblioteca de gestión de widget gráficos, que se mencionará en
  secciones posteriores) y se le inyecta dicho evento para que pueda
  gestionarlo. Por último, se añade el evento al buffer \texttt{events\_} y se
  elimina de él cualquier aparición del mismo tipo de evento pero con una
  condición de lanzamiento de liberado.

  La idea del método \texttt{remove\_key\_from\_buffer()} es la de eliminar los
  eventos generados por la pulsación de una misma tecla pero con una condición
  de lanzamiento contraria. En este caso, dado que se genera un evento con la
  condición de lanzamiento \texttt{OnKeyPressed}, se eliminarán los eventos con
  la misma tecla que pero con una condición de lanzamiento
  \texttt{OnKeyReleased}. De esta forma se evitan conflictos en la gestión de
  eventos.

\item La gestión de eventos finaliza con la invocación del método
  \texttt{check\_events()}. Este otro método iterará sobre el contenedor
  \texttt{events\_}, que almacena los eventos generados, y comprobará si alguno
  de ellos tiene asociado alguna acción; es decir, se comprueba si dicho evento
  tiene una entrada en el mapa \texttt{repeat\_triggers\_} (destinado a eventos
  de tipo \texttt{Repeat}) o en el mapa \texttt{do\_it\_once()} (destinado a
  eventos de tipo \texttt{DoItOnce}). De ser así se ejecutaría el callback
  asociado un número de veces que depende del tipo de evento que se acaba de
  mencionar.
\end{itemize}

Mediante el uso de un mapa que permite asociar teclas con functores de funciones
miembro, se elimina el problema de mantenibilidad e inflexibilidad que se tenía
anteriormente. Por un lado, añadir teclas asociadas a acciones es tan sencillo
como insertar una nueva entrada en el mapa anteriormente mencionado, tal y como
se muestra en el listado~\ref{list:mapeo-accion-callback}. Por otro, en lugar de
tener numerosas sentencias condicionales, se pueden comprobar si las teclas
pulsadas tienen alguna acción asociada comprobando si dicha tecla existe en el
mapa en forma de clave, y de ser así, se podría ejecutar la acción asociada.

En este proyecto se usa de forma extensiva el estándar 11 de C++ y en este
ejemplo se ve reflejado, sobretodo en el
listado~\ref{list:implementacion-input}. El par que se pasa como primer
argumento se crea usando la inicialización con llaves que proporciona
C++11. Esto aporta una mayor facilidad a la hora de programar ya que el
compilador crea automáticamente el par.

Además, se usa el algoritmo \texttt{find()} en el método
\texttt{remove\_key\_from\_buffer()} para encontrar la posición dentro del
buffer de eventos que ocupa el evento que se recibe por argumentos. Por último,
se puede ver el uso de la sentencia \texttt{auto} en el método
\texttt{trigger\_keyboard\_events()}, lo que ahorra tener que declarar un
iterador, al hacerse de forma implícita. Con esto se consigue tener un código
mas legible.

El código fuente de esta iteración se encuentra en la rama \emph{ogre} del
repositorio del proyecto\footnote{\url{https://bitbucket.org/arco_group/tfg.tinman/src/e58677d281d67e6a14aa86639eb832c171f4e488/src/?at=ogre}}.
