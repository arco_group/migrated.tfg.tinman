En esta iteración se va a implementar un mecanismo de instrumentación
que permita generar pruebas que simulen comportamientos típicos del
jugador de forma programática; en otras palabras, la instrumentación
proporcionará una manera programable de simular el comportamiento de
un jugador, de forma que se reduzca la carga de betatesting. Gracias a
esto, el tiempo de desarrollo puede ser reducido considerablemente, ya
que esta fase es una de las mas costosas.

\subsection{Análisis}
\label{sec:analisis-10}
Un videojuego debe tener el mínimo número de bugs posible, ya que
cualquier bug hace que el \emph{gameplay} se vea gravemente
afectado. Sin embargo, aunque es muy sencillo ver que el juego tiene
errores debido a los comportamientos erróneos que se visualizan en la
ejecucion del juego, encontrar la fuente del error no es nada
trivial. Los betatester puede pasarse semanas, e incluso meses,
probando el comportamiento del juego, comprobando condiciones en las
que ellos piensan que el juego puede fallar, ya que las personas
encargadas de esta fase de pruebas tienen experiencia realizando este
tipo de actividad.

La forma en que los betatester prueban el juego es la siguiente: a
través del controlador, empiezan a apretar los botones buscando que el
personaje se mueva hasta una parte concreta del escenario, accionando
alguna combinación de acciones del juego que puedan llevarlo a una
situación inconsistente; es decir, debido a que los desarrolladores
típicamente no tienen nada que les ayuden a saber en qué sección del
código se generan los errores, mas allá de las herramientas típicas de
depuración, los betatesters tienen que probar la mayor cantidad
posible de combinaciones de acciones, movimientos,... hasta que el
juego falle.

Una de las tácticas que suelen realizar los desarrolladores para
agilizar el proceso es generar escenarios donde se obligue al
personaje a ejecutar un tipo de acciones; por ejemplo, si se quiere
probar que el mecanismo de subida y bajada de escaleras funciona
correctamente, se genera un escenario lleno de escaleras. El problema
de este procedimiento es que no es automático y se tiene que realizar
la verificación manualmente, lo que es costoso en tiempo y recursos,
ya que no siempre es inmediato visualizar si existe algún error.

En la iteración~\ref{sec:it1} se dieron los primeros pasos en la
creación de pruebas automáticas añadiéndo un log que registraba una
serie de eventos que se consideraban relevantes. Las pruebas creadas
estaban basadas en este log, de forma que se esperaba encontrar una
serie de mensajes que informaran de acciones realizadas durante la
ejecución del juego. No encontrar esos mensajes era señal de que el
juego había fallado en un punto del código determinado por el mensaje
restante.

En esta iteración se pretende ir un paso mas allá, buscando crear
tests que simulen el comportamiento de un jugador humano que estuviese
probando una condición determinada del juego.

Para poder llevar a cabo esto, se hace necesario tener una forma
alternativa con la que interactuar con el videojuego, que permita a un
test realizar una serie de acciones típicas del usuario para el
videojuego que se está desarrollando en este proyecto: acelerar el
coche, hacerlo girar, frenarlo, usar los powerups, etcétera.

\subsection{Diseño e Implementación}
\label{sec:implementacion-10}
Hay que señalar que aunque la instrumentación es algo que lleva muchos
años realizándose en la industria del software, debido al carácter
reservado de la industria de los videojuegos, éste tipo de técnicas
ágiles no están muy divulgadas, de forma que hay un cierto matiz
innovador en esta iteración.

Dicho esto, la solución planteada aquí pasa por hacer uso de ZeroC
\acs{ICE} para permitir que una prueba invoque una serie de métodos
que se definirán en slice de ZeroC. Se expondrá el API pública de las
clases que se quieran instrumentar de forma que haciendo uso de los
mecanismos de red de ZeroC \acs{ICE} una prueba invoque dichos
métodos, de forma que programáticamente podrá simular el
comportamiento típico de un jugador. Por otra parte, la ventaja de
crear pruebas específicas para probar situaciones determinadas en el
juego es que, gracias a la flexibilidad que aporta la forma en que se
crean los circuitos, se puede lanzar la prueba con un circuito
determinado, hacer que la prueba ejecute una serie de acciones sobre
el coche, de la misma forma en que lo haría un jugador humano, y
después comprobar de forma automática alguna condición que se espera
que se cumpla.

A continuación se mostrará el proceso de creación de la primera prueba
creada, en la que simplemente se comprueba si el coche colisiona
contra uno de los muros del circuito, de forma que se ejemplifique el
proceso general que se ha seguido en todas las demás pruebas.

El primer paso que se ha realizado ha sido el de definir la interfaz
de ZeroC creando un archivo slice en el que se definen las
operaciones. En el listado~\ref{list:instrumentatedcar-slice} se puede
ver la definición de dicha interfaz. En él se pueden observar las
operaciones mas importantes de la clase Car y una serie de estructuras
para poder operar con los tipos de datos de Bullet, con los que
trabaja la clase Car.

\begin{listing}[language=C++,
  caption = {Slice que define la clase InsturmentatedCar},
  label = {list:instrumentatedcar-slice}]
#include <Ice/BuiltinSequences.ice>

module Tinman {
  enum Action {AccelerateBegin, AccelerateEnd,
               BrakeBegin, BrakeEnd,
               TurnRight, TurnLeft, TurnEnd,
               UseNitro};

  struct Quaternion {
    float x;
    float y;
    float z;
    float w;
  };

  struct Vector3 {
    float x;
    float y;
    float z;
  };


  interface InstrumentatedCar {
    void exec(Tinman::Action action);

    int getNitro();
    int getLap();
    Vector3 getPosition();
    Vector3 getVelocity();
    Quaternion getOrientation();
    Vector3 getDirection(int factor);
    float getSpeed();
    bool isColliding();
  };
};
  \end{listing}

  Tras esto, se compila el slice a C++ con la herramienta de ZeroC
  \emph{slice2cpp}. Después se ha de proporcionar una implementación
  para las operaciones definidas en la interfaz
  \emph{InstrumentatedCar}. Para ello, se ha creado la clase
  \emph{InstrumentatedCarI}, que hereda de las clases \emph{Car} y de
  \emph{InstrumentatedCar}, clase abstracta que autogenera
  \emph{slice2cpp} y que implementa una serie de operaciones de
  ZeroC. En el listado~\ref{list:icar} se puede ver el código fuente
  de esta clase. Lo mas relevante de la implementación radica en que
  por un lado esta clase redirige las invocaciones recibidas
  directamente a la clase Car, lo que demuestra lo sencilla que es la
  implementación, en contraste con la potencia que aporta este
  mecanismo. Por otro que se convierten los tipos de datos de Bullet a
  los tipos de datos definidos en el slice, por ejemplo en los método
  \texttt{getPosition()}.

  Al heredar de la clase \emph{Car} tambien comparte el
  mismo interfaz y atributos, de forma que el uso de \acs{ICE} es
  transparente para las demás clases. De esta forma, la prueba puede
  invocar las operaciones de la clase \emph{Car} de una forma similar
  a la que lo hace un jugador humano, con la diferencia de que este lo
  hace cuando presiona los botones del teclado y la prueba lo hace
  invocando métodos específicos para tal efecto. Hay que señalar que
  cuando una prueba invoca uno de los métodos definidos en el interfaz
  \emph{InstrumentatedCar} se sigue el mismo flujo de ejecución que
  cuando un jugador humano ejecuta la misma función como respuesta a
  presionar un botón del teclado.

Se ha creado tambien una clase \texttt{PlayerFactory}, con la idea de
abstraer la creación de los objetos personaje. La implementación de
esta clase se puede ver en el listado~\ref{list:PlayerFactory}. La
necesidad de crear esta factoria de jugadores radica en que además de
los tres tipos de controladores existentes en este punto del proyecto
(\emph{Human, Network y Bot}) hay que añadir uno nuevo el controlador
\emph{Instrumentado}.

Se ha añadido un modo de depuración al juego, en el que sólo se da la
opción de crear personajes con controladores de tipo \emph{Bot} y
\emph{Instrumentado}. Este modo de depuración se activa cuando se
ejecuta el juego con unos determinados argumentos por línea de
órdenes, que se verán a continuación. Un jugador instrumentado se
caracteriza por tener un coche instrumentado y un controlador
instrumentado. El controlador instrumentado es un controlador vacío
que únicamente invoca al método \texttt{car\_->update()} en su método
\texttt{update()}. Esto es así debido a que la idea de un
\texttt{InstrumentatedCar} es que las pruebas indiquen qué acciones se
quieren ejecutar, de forma que el controlador no es quien va a indicar
las acciones que ejecutará el coche, como ocurre con los otros tres
tipos de controlador, sino que el propio \texttt{InstrumentatedCar}
recibe las invocaciones desde la prueba.

Junto con la adición de la clase \texttt{PlayerFactory} se ha añadido
\texttt{CarFactory}. La implementación de esta clase se puede ver en
el listado~\ref{list:CarFactory}. Cuando el juego se arranca en modo
depuración, se crea un adaptador de objetos sobre el que se
registrarán los objetos de tipo \texttt{InstrumentatedCar} que creará
esta factoría, que es lo permite que una prueba invoque los métodos
del coche instrumentado.

Una vez que fue creado el soporte necesario para crear pruebas, el
siguiente paso consiste en crearlas. En el
listado~\ref{list:instrumentated-test} se puede ver el código de una
de las pruebas creadas. En esa prueba se quiere comprobar si los bots,
una vez se atascan contra una pared, son capaces de salir. Este era un
problema que existía en el proyecto y mediante la prueba mostrada se
consiguió eliminar.

Cabe destacar que la prueba se ha escrito en Python para poder
demostrar que en realidad no está accediendo directamente al motor de
juego, sino que interacciona con el juego a través de la clase
\emph{InstrumentatedCarI}. Además, de esa forma se ve la flexibilidad
que aporta utilizar \acs{ICE}, dado que no se está sujeto a un lenguje
determinado para crear pruebas.

\begin{listing}[language=C++,
    caption = {Prueba haciendo uso de la clase InsturmentatedCar.},
    label = {list:instrumentated-test}]
class RemoteGame(object):
    def __init__(self):
        self.cars = []
        self.broker = Ice.initialize()

    def add_car(self, proxy):
        proxy = self.broker.stringToProxy(proxy)
        self.cars.append(Tinman.InstrumentatedCarPrx.checkedCast(proxy))

    def get_car(self, index):
        return self.cars[index]


class TestBotDontGetStuck(unittest.TestCase):
    def setUp(self):
        server = SubProcess("./main --bots 1 --track config/circuit.data --frames 360", stdout=sys.stdout)
        server.start()
        wait_that(localhost, listen_port(10000))
        self.game = RemoteGame()
        self.game.add_car("Bot0: tcp -h 127.0.0.1 -p 10000")

        self.car0 = self.game.get_car(0)
        self.car0.move(Tinman.Vector3(-1, 2, -40))

        self.addCleanup(server.terminate)

    def test_right_1(self):
        time.sleep(5)
        self.assertFalse(self.car0.isStuck())
        wait_that(self.car0.isStuck, call_with().returns(True))

        time.sleep(2)
        self.assertFalse(self.car0.isStuck())

     def runTest(self):
        self.assertTrue(self.game.Icar.isColliding())
\end{listing}

En el \emph{SetUp} de la prueba, se lanza el juego como un
subproceso. Es necesario lanzar el juego dentro de la prueba de esta
forma debido a que como la ejecución del juego se realiza dentro de un
bucle infinito, hasta que este no finalizase la prueba quedaría
bloqueada.

Es necesario que la prueba pueda inicializar el juego
en un contexto determinado. Para ello, se ha añadido al juego una
serie de comandos por línea de órdenes. A través de ellos se puede
inicializar el juego con un circuito determinado, con una serie de
jugadores controlados por la prueba, con un número determinado de bots
e incluso se puede indicar al motor de juego que renderice las cajas
de contorno de las mallas 3D para realizar una depuración visual de la
geometría de la escena. Los argumentos añadidos en esta iteración son:
\begin{itemize}
\item \texttt{--debugRender}: permite renderizar las formas de
  colisión de los cuerpos físicos.
\item \texttt{--track <fichero>}: arranca el juego con un circuito
  determinado
\item \texttt{--players <número>}: crea un determinado número de
  jugadores instrumentados. Siempre menor o igual a 4.
\item \texttt{--bots <número>}: crea un determinado número de bots. La
  suma de los jugadores instrumentados y de los bots no puede superar
  4.
\item \texttt{--frames <número>}: el juego se ejecutará un número de
  frames.
\end{itemize}

La prueba del listado~\ref{list:instrumentated-test} hace uso de esta
característica del motor de juegos para indicarle el tipo de
circuito(`circuit.data'), el número de bots (1) y el número de frames
que se ejecutará el juego (360).

Una vez que se ha arrancado el juego, se crea un proxy del
\texttt{InstrumentatedCar} que utiliza el bot, usando el método
\texttt{add\_car} de la clase \texttt{RemoteGame}. Una vez creado, el
método hace uso de dicho proxy mediange el método
\texttt{get\_car(index)}.

El siguiente paso consiste en generar un contexto adecuado. En esta
prueba se quiere comprobar que los bots son capaces de chocar contra
una pared y continuar con la carrera de forma normal. Para generar
esta situación, se mueve el coche creado en la prueba hasta una
posición cercana a un muro y se deja que la \acs{IA} haga el resto. La
figura~\ref{fig:prueba-automatica} muestra el momento en que ha
finalizado el \texttt{SetUp} de la prueba y comienza a verificarse las
condiciones del test.

La solución al problema radicaba en mejorar la forma en que se
detectaban las colisiones contra los muros, algo que fue posible
detectar gracias a que con cada modificación en el algoritmo de
\acs{IA} se ejecutaba este test y se comprobaba el efecto que las
modificaciones realizadas sobre el código tenían sobre el algoritmo.

Esta es una de las ventajas que aportan los test automáticos: son
reproducibles con un coste cercano a cero (este test tarda 4 segundos
en ejecutarse), lo que agiliza mucho el proceso de desarrollo,
aportando feedback al ingeniero acerca de los cambios que está
realizando.

\begin{listing}[language=C++,
caption = {clase InstrumentatedCarI},
label = {list:icar}]
void
Tinman::InstrumentatedCarI::exec(::Tinman::Action action,
                                 const Ice::Current& current) {
  Car::exec(action);
}

::Ice::Int
Tinman::InstrumentatedCarI::getNitro(const Ice::Current& current) {

    return Car::get_nitro();
}

::Ice::Int
Tinman::InstrumentatedCarI::getLap(const Ice::Current& current) {
    return Car::get_lap();
}

::Tinman::Vector3
Tinman::InstrumentatedCarI::getPosition(const Ice::Current& current) {
  btVector3 position = Car::get_position();
  return {position.getX(), position.getY(), position.getZ()};
}

::Tinman::Vector3
Tinman::InstrumentatedCarI::getVelocity(const Ice::Current& current) {
  return {Car::get_velocity().x(), Car::get_velocity().y(),
      Car::get_velocity().z()};
}

::Tinman::Quaternion
Tinman::InstrumentatedCarI::getOrientation(const Ice::Current& current) {
  return {Car::get_orientation().getX(),
      Car::get_orientation().getY(),
      Car::get_orientation().getZ(),
      Car::get_orientation().getW()};
}

::Tinman::Vector3
Tinman::InstrumentatedCarI::getDirection(::Ice::Int factor,
                                         const Ice::Current& current) {
  return {Car::get_direction().x(), Car::get_direction().y(),
                                    Car::get_direction().z()};
}

::Ice::Float
Tinman::InstrumentatedCarI::getSpeed(const Ice::Current& current) {
    return Car::get_speed();
}

bool
Tinman::InstrumentatedCarI::isColliding(const Ice::Current& current) {
    return Car::is_colliding();
}

bool
Tinman::InstrumentatedCarI::isStuck(const Ice::Current& current) {
  return is_stuck();
}

void
Tinman::InstrumentatedCarI::move(const ::Tinman::Vector3& position,
                                 const Ice::Current& current) {
  btVector3 new_position(position.x, position.y, position.z);
  set_position(new_position);
}
\end{listing}

\begin{listing}[language=C++,
caption = {clase PlayerFactory},
label = {list:PlayerFactory}]
Player::shared
HumanFactory::create(std::string nick, EventListener::shared input,
                     Sound::shared sound, Car::shared car) {
  HumanController::KeyBinding player_bindings {
    {OIS::KC_W, {Tinman::Action::AccelerateBegin, Tinman::Action::AccelerateEnd}},
    {OIS::KC_S, {Tinman::Action::BrakeBegin,  Tinman::Action::BrakeEnd}},
    {OIS::KC_A, {Tinman::Action::TurnLeft,  Tinman::Action::TurnEnd}},
    {OIS::KC_D, {Tinman::Action::TurnRight,  Tinman::Action::TurnEnd}},
    {OIS::KC_SPACE, {Tinman::Action::UseNitro,  Tinman::Action::AccelerateEnd}}
  };

  Controller::shared human_controller =
    std::make_shared<HumanController>(input, player_bindings, car);
  return std::make_shared<Player>(nick, human_controller, sound);
}

Player::shared
BotFactory::create(std::string nick, Physics::shared physics,
           Sound::shared sound, Race::shared race, Car::shared car){
  BotController::shared bot = std::make_shared<BotController>(physics, car);
  std::dynamic_pointer_cast<BotController>(bot)->add_race(race);
  return std::make_shared<Player>(nick, bot, sound);
}

Player::shared
NetworkFactory::create(std::string nick, Sound::shared sound, Car::shared car){
  NetworkController::shared controller = std::make_shared<NetworkController>(car);
  return std::make_shared<Player>(nick, controller, sound);
}

Player::shared
InstrumentatedFactory::create(std::string nick, Sound::shared sound,
                              Car::shared car) {
  std::cout << "[InstrumentatedFactory] create" << std::endl;
  IController::shared controller = std::make_shared<IController>(car);
  return std::make_shared<Player>(nick, controller, sound);
}
\end{listing}

\begin{listing}[language=C++,
caption = {clase CarFactory},
label = {list:CarFactory}]
CarFactory::CarFactory(Ice::ObjectAdapterPtr adapter) {
  adapter_ = adapter;
  instrumentated_ = true;
}

Car::shared
CarFactory::create(std::string nick) {
  if(!instrumentated_)
	return std::make_shared<Car>(nick);

  auto car = std::make_shared<Tinman::InstrumentatedCarI>(nick);
  Ice::ObjectPrx peer_proxy = adapter_->add(car.get(),
				adapter_->getCommunicator()->stringToIdentity(nick));
  auto peer = Tinman::InstrumentatedCarPrx::checkedCast(peer_proxy->ice_twoway());
  return car;
}
\end{listing}

\begin{figure}[h]
  \centering
  \includegraphics[width=12cm]{prueba-automatica.png}
  \caption{Resultado de la iteración 10}
  \label{fig:prueba-automatica}
\end{figure}


% Local Variables:
%   coding: utf-8
%   fill-column: 90
%   mode: flyspell
%   ispell-local-dictionary: "american"
%   mode: latex
%   TeX-master: "main"
% End:
