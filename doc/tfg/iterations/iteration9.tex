En este nuevo incremento del proyecto se persigue implementar un algoritmo de
inteligencia artificial que permita que un jugador controlado por la máquina
pueda dar una vuelta a un circuito, en un tiempo razonable, independientemente
de la topología del mismo. Por otra parte, se debe permitir a la máquina
esquivar de alguna forma los obstáculos que se interpongan en su camino, como
otros coches que se hallen en mitad del circuito, las paredes del mismo,
etcétera.

\subsection{Análisis}
\label{sec:analisis-9}
A la hora de implementar el algoritmo de inteligencia artificial para este
proyecto hay una serie de problemas que se deben resolver:
\begin{itemize}
\item se debe detectar obstáculos dinámicamente
\item debe ser capaz de adaptarse a diferentes tipos de circuitos.
\end{itemize}

Los algoritmos de inteligencia artificial de los videojuegos comerciales de
carreras típicamente utilizan un algoritmo A* para buscar la ruta óptima del
circuito, definiendo una serie de waypoints a lo largo del circuito como espacio
de búsqueda. Sin embargo, típicamente estos algoritmos son computacionalmente
muy costosos, ya que buscan a lo largo de todo el espacio de búsqueda, llegando
a definir cientos de waypoints si se trata de juegos realistas.

Además, este tipo de algoritmos no tienen en cuenta los posibles obstáculos
dinámicos que pueda haber a lo largo de la carrera, como por ejemplo los otros
coches.

Para reducir la complejidad del algoritmo de inteligencia artificial, se decidió
seguir una aproximación un tanto diferente a la del algoritmo A*. Aunque sí que
se define un espacio de búsqueda, siendo este los diferentes segmentos que
conforman el circuito, se ha decidido que el vehículo realice una detección
dinámica de obstáculos combinada con una serie de acciones estáticas dependiendo
del tipo de segmento en el que se encuentre el vehículo.

\subsection{Diseño e Implementación}
\label{sec:implementacion-9}
Teniendo en cuenta todas las consideraciones anteriores, se decidió diseñar el
algoritmo en función de dos factores:
\begin{itemize}
\item si el vehículo tiene algún obstáculo delante a una distancia
  suficientemente cercana,
\item el tipo de segmento sobre el que esté posicionado en cada momento.
\end{itemize}

En el listado~\ref{list:BotController} se muestra la implementación de la clase
\texttt{BotController}, clase donde se encapsula el algoritmo que se explicará a
continuación. Dicha clase es otro tipo de controlador que se ha creado para
evitar acoplar el algoritmo de \acs{IA} a la clase \texttt{Player}.

Para detectar obstáculos o colisiones se utilizan dos métodos. Por un lado, se
lanzan rayos hacia delante, hacia la izquierda y la derecha rotando el primer
rayo +-15 grados. Si el primer rayo colisiona, es un indicador de que hay un obstáculo
delante del vehículo, mientras que si el rayo de la derecha o el de la izquierda
colisionan significa que el vehículo está cerca de un muro.

A la hora de lanzar los rayos hay que tener en cuenta que los segmentos del
circuito tienen pequeñas oscilaciones en el terreno, así como hay otros tipos de
segmentos que son rampas de una altura similar a la del del coche. Para evitar que
los rayos choquen contra objetos que no son obstáculos, hay que lanzarlos unas
unidades por encima del eje perpendicular del plano que define el terreno. Por
esta razón, no se pueden detectar colisiones contra otros vehículos usando este
método. Para ello, se ha tenido que hacer una asunción: si el vehículo está
acelerando y la velocidad de este no supera un valor mínimo, significa que el
vehículo está atascado.

Cuando el algoritmo detecta alguna de estas condiciones, realiza la acción
correspondiente:
\begin{itemize}
\item Si el rayo lanzado en la dirección en que está orientado el vehículo ha
  colisionado, el algoritmo hará acelerar marcha atrás hasta que el rayo deje de
  colisionar.
\item Si el rayo derecho o izquierdo colisionan, significa que el vehículo está
  cerca de un muro bien por la parte de la derecha o de la izquierda. En este
  caso, el algoritmo hará que el vehículo gire en la dirección contraria a la del
  rayo que está colisionando.
\item Si el vehículo está acelerando y la velocidad de éste no supera un cierto
  valor, significa que este está atascado, de modo que el algoritmo hará que el
  vehículo acelere en sentido contrario.
\end{itemize}

Por otra parte, si el algoritmo no detecta ningún obstáculo, realizará una serie
de acciones dependiendo del tipo de segmento sobre el que se halle el vehículo:
\begin{itemize}
\item Si se trata de una recta o de la meta, acelerará.
\item Si se trata de una curva, girará en el sentido de la curva.
\end{itemize}

A la hora de girar, dependiendo del sentido en que avance el vehículo, el giro
puede ser hacia la derecha o hacia la izquierda; en otras palabras, la dirección
de giro depende del tipo de segmento que haya antes de la curva. Para facilitar
la identificación del tipo de segmento se ha agrupado cada pieza dependiendo de
su malla. En la figura~\ref{fig:circuito-ejemplo} se muestran todos los tipos de
segmentos y en el listado~\ref{list:declaracion-track} se puede ver la
estructura \emph{Section}.

A grandes rasgos, el algoritmo detecta la dirección de giro a partir de estas reglas:
\begin{itemize}
\item Si el vehículo se halla en una curva de tipo \emph{RightCurve1} y el
  segmento anterior era horizontal, girará a la derecha. Si por el contrario era
  un segmento vertical, girará a la izquierda. Se entiende que los segmentos
  verticales son recta2, rampa2 o cualquier curva que pueda acoplarse de forma
  vertical. De forma similar, se entiende la horizontalidad de los tipos de segmento.
\item Si se encuentra en una curva de tipo \emph{RightCurve2}, el vehículo
  girará a la derecha si el segmento anterior era vertical y hacia la izquierda
  si era un segmento horizontal.
\item Si el segmento actual es de tipo \emph{LeftCurve1} girará a la izquierda
  si el segmento anterior es horizontal y hacia la derecha si el segmento
  anterior es vertical.
\item Por último, si se trata de una curva de tipo \emph{LeftCurve2} girará a la
  izquierda si el segmento anterior es vertical y hacia la derecha si es horizontal.
\end{itemize}

Por último, para que el vehículo pueda tener la información acerca del segmento
de circuito sobre el que se encuentra en cada momento, se hace uso de una
utilidad creada específicamente para este proyecto. Aprovechándonos de la
información que proporciona Bullet en la fase \emph{Broadphase} de su pipeline
físico, se utilizan los pares de colisión que detecta para comprobar si alguno
de ellos corresponde con algún cuerpo físico del cuál se quiera realizar alguna
acción en el momento que se detecte la colisión. Para ello, se registra el
cuerpo físico del coche junto con cada uno de los cuerpos físicos de los
segmentos del circuito. En el listado~\ref{list:register-collision-pair} se ve
un código similar, en el que se registra una colisión entre el coche y un
powerup. Se delega la labor de detección de colisiones del vehículo a la clase
\emph{Physics}, la cuál es un wrapper que encapsula llamadas a \emph{Bullet
  Physics}.

\begin{listing}[language=C++,
  caption = {Clase BotController},
  label = {list:BotController}]
void
BotController::update(float delta) {
  btVector3 position = car_->get_position();
  position.setY(5);
  btVector3 direction = car_->get_direction(4);
  direction.setY(5);
  btVector3 next_waypoint = get_next_waypoint(index_);
  next_waypoint.setY(5);

  auto callback = physics_->raytest(position, direction);
  if(callback.hasHit())
    correct_position();
  else
    go_to_waypoint(next_waypoint);

  car_->update(delta);
}

void
BotController::go_to_waypoint(btVector3 next_destination) {

  exec(Tinman::AccelerateBegin);
  if(car_->current_segment_.type == Section::Meta ||
     car_->current_segment_.type == Section::Rect) {
    exec(Tinman::TurnEnd);
    return;
  }

  if(car_->current_segment_.type == Section::RightCurve) {
    exec(Tinman::TurnRight);
    return;
  }

    if(car_->current_segment_.type == Section::LeftCurve) {
    exec(Tinman::TurnLeft);
    return;
  }
}

btVector3
BotController::get_next_waypoint(int index) {
  return car_->current_segment_.position;
}

void
BotController::correct_position() {
  btVector3 position = car_->get_position();
  position.setY(5);
  btVector3 direction = car_->get_direction(4);
  direction.setY(5);

  btVector3 right_direction = direction.rotate(btVector3(0,1,0), degree_to_radians(20));
  btVector3 left_direction = direction.rotate(btVector3(0,1,0), degree_to_radians(-20));
  auto right_ray = physics_->raytest(position, right_direction);
  auto left_ray = physics_->raytest(position, left_direction);

  if(right_ray.hasHit() && left_ray.hasHit()) {
    exec(Tinman::AccelerateEnd);
    car_->exec(Tinman::BrakeBegin);
  }

  if(right_ray.hasHit()) {
    exec(Tinman::AccelerateBegin);
    exec(Tinman::TurnLeft);
    return;
  }

  if(left_ray.hasHit()){
    exec(Tinman::AccelerateBegin);
    exec(Tinman::TurnRight);
    return;
  }
}

void
BotController::exec(Tinman::Action action) {
  car_->exec(action);
  observer_->notify(action);
}
\end{listing}
