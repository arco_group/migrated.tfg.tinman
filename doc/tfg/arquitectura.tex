\chapter{Arquitectura del Motor}
\label{cha:arqu-del-motor}
\drop{A} raíz del desarrollo del videojuego se ha ido creando una serie de
clases de utilidad con el propósito de solucionar los problemas que
han ido surgiendo durante el transcurso del proyecto. Debido al
esfuerzo invertido en que el diseño sea flexible y en que la lógica de
control esté lo mas desacoplada posible de los modelos de datos, se ha
conseguido crear una primera aproximación a lo que será un motor de
juegos propio.

En este capitulo se explicará cuál es la jerarquía de clases que
conforman el motor de juego de este proyecto. Las clases mas
importantes del proyecto son las siguientes:
\begin{itemize}
\item \textbf{Clase Game}: clase principal. Contiene referencias a
  todos los gestores y alberga el bucle de juego. Al crear una
  instancia de esta clase se inicializa el motor de juego.
\item \textbf{Gestores de utilidades}: Son clases de utilidad que
  ayudan a gestionar los elementos de la escena, los cuerpos físicos,
  los efectos de sonido, la interfaz gráfica, etcétera.
\item \textbf{Jerarquía de estados}: Un estado es una abstracción que define el
  comportamiento del juego en un momento determinado. Gracias a esta abstracción
  es posible desacoplar la lógica de control del bucle principal del juego.
\item \textbf{Jerarquía de controladores}: Un controlador es una clase que permite
  desacoplar la lógica de control de la clase Player. Gracias a esto, es posible
  crear de forma transparente diferentes tipos de jugadores.
\item \textbf{Sesión de juego}: Clase que modela una sesión de juego.
\item \textbf{Clase Race y Track}: La clase race sirve como interfaz sobre la que obtener
  información relativa a la carrera. La clase Track representa un circuito.
\item \textbf{Clase Car}: encapsula la información relativa al vehículo.
\end{itemize}

En la siguientes secciones explicará detalladamente algunos de los
aspectos mas importantes de las clases que conforman el listado
anterior.

\section{Clase Game}
\label{sec:clase-game}

Es la clase principal del juego. Al crear una instancia se inicializan
cada uno de los gestores de utilidad que se usarán para inicializar
los demás componentes del juego. Únicamente hay que crear una única
instancia de esta clase por cada juego que se quiera ejecutar. Cuando
una clase del motor de juego necesita tener acceso a alguno de los
gestores, este es inyectado a través del constructor de dicha
clase. De esta forma, se tiene un lugar específico donde se crean las
instancias de las clases que representan el núcleo del motor del
juego.

Una vez que se llama al método \emph{start} de esta clase, se inicia
el proceso de arranque del juego, renderizándose el menú principal en
la ventana que se crea para mostrar el juego para, acto seguido,
comenzar a ejecutar el bucle principal del juego.

Es importante señalar que el bucle principal del juego es la
estructura de control mas importante de este. Desde ese punto es donde
se ejecutará cualquier tipo de lógica que sea necesaria en él. En el
listado~\ref{list:game-loop} se puede ver el código fuente del mismo.

\begin{listing}[language=C++,
  caption = {Instrucciones que se ejecutan en el bucle de juego.},
  label = {list:game-loop}]
  void
  Game::step() {
    delta_ += timer_.get_delta_time();
    input_->capture();
    if(delta_ >= (1/FPS)) {
      input_->check_events();
      state_machine_->get_current_state()->update(delta_);
      scene_->render_one_frame();
      delta_ = 0.f;
    }
  }
\end{listing}

Una vez que se destruye la instancia de esta clase, el juego se cierra.

\section{Gestores de utilidad}
\label{sec:gestores-de-utilidad}

Este conjunto de clases son en realidad una serie de wrappers que
encapsulan la funcionalidad de las diversas bibliotecas que se han
usado durante el desarrollo del proyecto. Estas son:
\begin{itemize}
\item Scene: encapsula la funcionalidad de Ogre3D. Implementa una
  serie de funciones que facilitan la creación de elementos típicos en
  una escena de un videojuego: planos, nodos de escena y entidades de
  Ogre, luces, cámaras. Facilita realizar operaciones geométricas
  sobre los nodos de escena, así como añadir o eliminar nodos al grafo
  de escena, añadir nuevos viewports a la cámara, añadir luces, el tipo de algoritmo de generación de sombras, etcétera.
\item Physics: encapsula llamadas a Bullet Physics. Facilita realizar
  operaciones sobre cuerpos rígidos(movimiento, rotación, aplicación
  de fuerzas, etcétera) y creación de diversas formas de colisión. El
  módulo de dinámica de vehículos de Bullet Physics facilita la
  implementación de la Car.
\item EventListener: Clase que permite gestionar los eventos de
  teclado y ratón. Permite añadir callbacks asociados a un evento de
  pulsación o levantado de una tecla, bien de teclado o de ratón. A la
  hora de detectar qué teclas se han pulsados, se hace uso de la
  funcionalidad ofrecida por la biblioteca OIS.
\item GUI: clase que encapsula funciones de creación de Overlays de
  Ogre3D, así llamadas de CEGUI que hacen mas sencilla la creación de
  elementos para la interfaz gráfica: cuadros de texto, botones,
  ventanas, barras de progreso, etcétera.
\item Sound: clase que encapsula llamadas a la biblioteca \acf{SDL}. Se
  encarga de gestionar los recursos de sonido del videjuego: ejecutar
  efectos de sonido, bucles musicales, cargar ficheros de sonido para
  que sean utilizables por el videojuego, etcétera.
\end{itemize}

Estas clases permiten reducir el tiempo de desarrollo en nuevos
proyectos, ya que ofrecen una serie de operaciones muy comunes que se
hacen necesarias durante el desarrollo de un videojuego, que las
bibliotecas originales no implementan. Por ejemplo, a la hora de crear
un escenario, las clase \emph{Scene} y \emph{Physics} facilitan la
labor de gestionar los cuerpos que la componen.

\section{Jerarquía de estados}
\label{sec:jerarquia-de-estados}

Un videojuego es un programa complejo, el cual tiene una serie de
comportamientos diferentes dependiendo del punto del videojuego en que
se encuentre el jugador. Por ejemplo, si este está en el menú
principal, el juego deberá darle acceso al jugador a los ranking, a un
menú de opciones, así como de comenzar la partida, todo esto a través
de una serie de botones. Si se encuentra en mitad de la partida, el
juego deberá darle la posibilidad al jugador de controlar su vehículo.

Por tanto, existe un problema: se tiene una gran variedad de
comportamientos que se deben gestionar dependiendo del contexto actual
del videojuego en cada momento. Debido a que toda la lógica del
videojuego se ejecuta en el bucle principal, en principio la lógica
que define cada uno de estos comportamientos y la lógica de control
que permite seleccionar un comportamiento u otro deberá estar acoplada
en el bucle principal. Para poder resolver este problema se decidió
usar una clase abstracta que, heredando de ella, permitiese encapsular
diferentes tipos de lógica, de forma que fuese posible desacoplarla
del bucle principal.

En el listado~\ref{list:game-loop} se muestra el mecanismo que permite
desacoplar la lógica de control del bucle de juego. La variable
\emph{state\_machine\_} es una instancia de la clase
\emph{StateMachine}. Esta clase encapsula la lógica necesaria para
poder cambiar entre los diferentes estados del juego, ademas de dar
acceso a los propios estados.

El método update de la clase \emph{State} está pensado para actualizar
la lógica del juego de forma transparente. Cada uno de los estados
implementará dicho método de forma que, dependiendo del estado actual,
se ejecutará una lógica u otra; por ejemplo, en el update del estado
\emph{Play} se actualiza el estado de la sesión de juego, mientras que
en el update del estado \emph{Results} se actualizan los rankings. En
la figura~\ref{fig:estados-uml} se puede ver la jerarquía de
estados.

\begin{figure}[h]
  \centering
  \includegraphics[width=0.8\textwidth]{jerarquia-estados.png}
  \caption[Diagrama \acs{UML} de la Jerarquía de estados]{Diagrama UML de la Jerarquía de estados}
  \label{fig:estados-uml}
\end{figure}


\section{Controladores de jugador}
\label{sec:contr-de-jugad}

De forma similar al problema de los estados, existe un problema relativo a los
jugadores. En el juego que se está desarrollando existen tres perfiles distintos
que definen el comportamiento de un jugador:
\begin{itemize}
\item Human: este tipo de jugador ejecuta sus acciones en respuesta a eventos
  de teclado, ratón o de un joystick.
\item Bot: es un jugador que es controlado por un algoritmo de inteligencia
  artificial.
\item Network: este tipo de jugador representa a un jugador \emph{remoto} dentro
  de una partida en red. Se dice que un jugador es \emph{remoto} cuando la
  instancia del jugador no se encuentra en la misma máquina que la que está
  usando el jugador humano; es decir, en una partida en red, los jugadores
  remotos son aquellos que no son el jugador humano y que no son bots.
\end{itemize}

De la misma forma que en la sección~\ref{sec:jerarquia-de-estados}, existe el
problema de desacoplar la lógica de control del propio jugador. Para esto, se ha
creado un clase \emph{Controller}. El objetivo de esta clase es encapsular la
lógica de control. La clase \emph{Player} encapsula una instancia de dicha
clase.

La clase \emph{Controller}, al igual que la clase \emph{Player}, es una clase
abstracta. Esto permite realizar diversas implementaciones de dicha clase
dependiendo de las necesidades. En la figura~\ref{fig:jerarquia-controladores}
se muestra la jerarquía de controladores.

\begin{figure}[h]
  \centering
  \includegraphics[width=8cm]{jerarquia-controladores.png}
  \caption{Jerarquía de controladores}
  \label{fig:jerarquia-controladores}
\end{figure}

\section{Clase Sesión}
\label{sec:sesion-de-juego}

Esta clase representa una sesión de juego. Encapsula la funcionalidad
necesaria para iniciar una carrera, crear todos los jugadores
necesarios, gestionar los tiempos de cada vuelta, la puntuación de los
jugadores y ejecutar la simulación física de la carrera.

\section{Clase Car}
\label{sec:clase-car}

Esta clase inicializa un vehículo de Bullet Physics. Para desacoplar
la lógica de control de la clase Car, se implementa un patrón Command
que invoca funciones de una clase \emph{CarController}. Dicha clase
tiene el mismo objetivo que los controladores de jugador, desacoplar
la lógica de control de los modelos de datos.
% Local Variables:
%   coding: utf-8
%   fill-column: 90
%   mode: flyspell
%   ispell-local-dictionary: "american"
%   mode: latex
%   TeX-master: "main"
% End:
